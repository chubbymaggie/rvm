# RVM

Rapid Vulnerability Mitigation with Security Workarounds

Rapid Vulnerability Mitigation (RVM) is a tool that automatically generates Security Workarounds for Rapid Response (SWRRs) and instruments SWRRs into a binary to mitigate vulnerabilities rapidly. 
