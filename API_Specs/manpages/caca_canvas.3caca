.TH "caca_canvas" 3caca "Sun Feb 7 2016" "Version 0.99.beta19" "libcaca" \" -*- nroff -*-
.ad l
.nh
.SH NAME
caca_canvas \- libcaca canvas drawing
.SS "Macros"

.in +1c
.ti -1c
.RI "#define \fBCACA_MAGIC_FULLWIDTH\fP   0x000ffffe"
.br
.in -1c
.SS "Functions"

.in +1c
.ti -1c
.RI "__extern int \fBcaca_gotoxy\fP (\fBcaca_canvas_t\fP *, int, int)"
.br
.RI "\fISet cursor position\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_wherex\fP (\fBcaca_canvas_t\fP const *)"
.br
.RI "\fIGet X cursor position\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_wherey\fP (\fBcaca_canvas_t\fP const *)"
.br
.RI "\fIGet Y cursor position\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_put_char\fP (\fBcaca_canvas_t\fP *, int, int, uint32_t)"
.br
.RI "\fIPrint an ASCII or Unicode character\&. \fP"
.ti -1c
.RI "__extern uint32_t \fBcaca_get_char\fP (\fBcaca_canvas_t\fP const *, int, int)"
.br
.RI "\fIGet the Unicode character at the given coordinates\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_put_str\fP (\fBcaca_canvas_t\fP *, int, int, char const *)"
.br
.RI "\fIPrint a string\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_printf\fP (\fBcaca_canvas_t\fP *, int, int, char const *,\&.\&.\&.)"
.br
.RI "\fIPrint a formated string\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_vprintf\fP (\fBcaca_canvas_t\fP *, int, int, char const *, va_list)"
.br
.RI "\fIPrint a formated string (va_list version)\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_clear_canvas\fP (\fBcaca_canvas_t\fP *)"
.br
.RI "\fIClear the canvas\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_set_canvas_handle\fP (\fBcaca_canvas_t\fP *, int, int)"
.br
.RI "\fISet cursor handle\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_get_canvas_handle_x\fP (\fBcaca_canvas_t\fP const *)"
.br
.RI "\fIGet X handle position\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_get_canvas_handle_y\fP (\fBcaca_canvas_t\fP const *)"
.br
.RI "\fIGet Y handle position\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_blit\fP (\fBcaca_canvas_t\fP *, int, int, \fBcaca_canvas_t\fP const *, \fBcaca_canvas_t\fP const *)"
.br
.RI "\fIBlit a canvas onto another one\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_set_canvas_boundaries\fP (\fBcaca_canvas_t\fP *, int, int, int, int)"
.br
.RI "\fISet a canvas' new boundaries\&. \fP"
.in -1c
.SH "Detailed Description"
.PP 
These functions provide low-level character printing routines and higher level graphics functions\&. 
.SH "Macro Definition Documentation"
.PP 
.SS "#define CACA_MAGIC_FULLWIDTH   0x000ffffe"
Used to indicate that the previous character was a fullwidth glyph\&. 
.PP
Referenced by caca_blit(), caca_flip(), caca_put_attr(), caca_put_char(), and caca_rotate_180()\&.
.SH "Function Documentation"
.PP 
.SS "__extern int caca_gotoxy (\fBcaca_canvas_t\fP * cv, int x, int y)"
Put the cursor at the given coordinates\&. Functions making use of the cursor will use the new values\&. Setting the cursor position outside the canvas is legal but the cursor will not be shown\&.
.PP
This function never fails\&.
.PP
\fBParameters:\fP
.RS 4
\fIcv\fP A handle to the libcaca canvas\&. 
.br
\fIx\fP X cursor coordinate\&. 
.br
\fIy\fP Y cursor coordinate\&. 
.RE
.PP
\fBReturns:\fP
.RS 4
This function always returns 0\&. 
.RE
.PP

.PP
Referenced by caca_conio_cgets(), caca_conio_clrscr(), caca_conio_cprintf(), caca_conio_cputs(), caca_conio_gotoxy(), caca_conio_printf(), and caca_conio_putch()\&.
.SS "__extern int caca_wherex (\fBcaca_canvas_t\fP const * cv)"
Retrieve the X coordinate of the cursor's position\&.
.PP
This function never fails\&.
.PP
\fBParameters:\fP
.RS 4
\fIcv\fP A handle to the libcaca canvas\&. 
.RE
.PP
\fBReturns:\fP
.RS 4
The cursor's X coordinate\&. 
.RE
.PP

.PP
Referenced by caca_conio_cgets(), caca_conio_clreol(), caca_conio_cprintf(), caca_conio_cputs(), caca_conio_printf(), caca_conio_putch(), and caca_conio_wherex()\&.
.SS "__extern int caca_wherey (\fBcaca_canvas_t\fP const * cv)"
Retrieve the Y coordinate of the cursor's position\&.
.PP
This function never fails\&.
.PP
\fBParameters:\fP
.RS 4
\fIcv\fP A handle to the libcaca canvas\&. 
.RE
.PP
\fBReturns:\fP
.RS 4
The cursor's Y coordinate\&. 
.RE
.PP

.PP
Referenced by caca_conio_cgets(), caca_conio_clreol(), caca_conio_cprintf(), caca_conio_cputs(), caca_conio_printf(), caca_conio_putch(), and caca_conio_wherey()\&.
.SS "__extern int caca_put_char (\fBcaca_canvas_t\fP * cv, int x, int y, uint32_t ch)"
Print an ASCII or Unicode character at the given coordinates, using the default foreground and background colour values\&.
.PP
If the coordinates are outside the canvas boundaries, nothing is printed\&. If a fullwidth Unicode character gets overwritten, its remaining visible parts are replaced with spaces\&. If the canvas' boundaries would split the fullwidth character in two, a space is printed instead\&.
.PP
The behaviour when printing non-printable characters or invalid UTF-32 characters is undefined\&. To print a sequence of bytes forming an UTF-8 character instead of an UTF-32 character, use the \fBcaca_put_str()\fP function\&.
.PP
This function returns the width of the printed character\&. If it is a fullwidth character, 2 is returned\&. Otherwise, 1 is returned\&.
.PP
This function never fails\&.
.PP
\fBParameters:\fP
.RS 4
\fIcv\fP A handle to the libcaca canvas\&. 
.br
\fIx\fP X coordinate\&. 
.br
\fIy\fP Y coordinate\&. 
.br
\fIch\fP The character to print\&. 
.RE
.PP
\fBReturns:\fP
.RS 4
The width of the printed character: 2 for a fullwidth character, 1 otherwise\&. 
.RE
.PP

.PP
References caca_add_dirty_rect(), CACA_MAGIC_FULLWIDTH, and caca_utf32_is_fullwidth()\&.
.PP
Referenced by caca_conio_cgets(), caca_conio_cputs(), caca_conio_putch(), caca_dither_bitmap(), caca_fill_box(), caca_fill_triangle(), caca_flush_figlet(), caca_put_figchar(), and caca_put_str()\&.
.SS "__extern uint32_t caca_get_char (\fBcaca_canvas_t\fP const * cv, int x, int y)"
Get the ASCII or Unicode value of the character at the given coordinates\&. If the value is less or equal to 127 (0x7f), the character can be printed as ASCII\&. Otherise, it must be handled as a UTF-32 value\&.
.PP
If the coordinates are outside the canvas boundaries, a space (0x20) is returned\&.
.PP
A special exception is when CACA_MAGIC_FULLWIDTH is returned\&. This value is guaranteed not to be a valid Unicode character, and indicates that the character at the left of the requested one is a fullwidth character\&.
.PP
This function never fails\&.
.PP
\fBParameters:\fP
.RS 4
\fIcv\fP A handle to the libcaca canvas\&. 
.br
\fIx\fP X coordinate\&. 
.br
\fIy\fP Y coordinate\&. 
.RE
.PP
\fBReturns:\fP
.RS 4
The Unicode character at the given coordinates\&. 
.RE
.PP

.PP
Referenced by caca_flush_figlet(), and caca_put_figchar()\&.
.SS "__extern int caca_put_str (\fBcaca_canvas_t\fP * cv, int x, int y, char const * s)"
Print an UTF-8 string at the given coordinates, using the default foreground and background values\&. The coordinates may be outside the canvas boundaries (eg\&. a negative Y coordinate) and the string will be cropped accordingly if it is too long\&.
.PP
See \fBcaca_put_char()\fP for more information on how fullwidth characters are handled when overwriting each other or at the canvas' boundaries\&.
.PP
This function returns the number of cells printed by the string\&. It is not the number of characters printed, because fullwidth characters account for two cells\&.
.PP
This function never fails\&.
.PP
\fBParameters:\fP
.RS 4
\fIcv\fP A handle to the libcaca canvas\&. 
.br
\fIx\fP X coordinate\&. 
.br
\fIy\fP Y coordinate\&. 
.br
\fIs\fP The string to print\&. 
.RE
.PP
\fBReturns:\fP
.RS 4
The number of cells printed\&. 
.RE
.PP

.PP
References caca_put_char(), caca_utf32_is_fullwidth(), and caca_utf8_to_utf32()\&.
.PP
Referenced by caca_vprintf()\&.
.SS "__extern int caca_printf (\fBcaca_canvas_t\fP * cv, int x, int y, char const * format,  \&.\&.\&.)"
Format a string at the given coordinates, using the default foreground and background values\&. The coordinates may be outside the canvas boundaries (eg\&. a negative Y coordinate) and the string will be cropped accordingly if it is too long\&. The syntax of the format string is the same as for the C printf() function\&.
.PP
This function returns the number of cells printed by the string\&. It is not the number of characters printed, because fullwidth characters account for two cells\&.
.PP
This function never fails\&.
.PP
\fBParameters:\fP
.RS 4
\fIcv\fP A handle to the libcaca canvas\&. 
.br
\fIx\fP X coordinate\&. 
.br
\fIy\fP Y coordinate\&. 
.br
\fIformat\fP The format string to print\&. 
.br
\fI\&.\&.\&.\fP Arguments to the format string\&. 
.RE
.PP
\fBReturns:\fP
.RS 4
The number of cells printed\&. 
.RE
.PP

.PP
References caca_vprintf()\&.
.SS "__extern int caca_vprintf (\fBcaca_canvas_t\fP * cv, int x, int y, char const * format, va_list args)"
Format a string at the given coordinates, using the default foreground and background values\&. The coordinates may be outside the canvas boundaries (eg\&. a negative X coordinate) and the string will be cropped accordingly if it is too long\&. The syntax of the format string is the same as for the C vprintf() function\&.
.PP
This function returns the number of cells printed by the string\&. It is not the number of characters printed, because fullwidth characters account for two cells\&.
.PP
This function never fails\&.
.PP
\fBParameters:\fP
.RS 4
\fIcv\fP A handle to the libcaca canvas\&. 
.br
\fIx\fP X coordinate\&. 
.br
\fIy\fP Y coordinate\&. 
.br
\fIformat\fP The format string to print\&. 
.br
\fIargs\fP A va_list containting the arguments to the format string\&. 
.RE
.PP
\fBReturns:\fP
.RS 4
The number of cells printed\&. 
.RE
.PP

.PP
References caca_put_str()\&.
.PP
Referenced by caca_conio_cprintf(), caca_conio_printf(), and caca_printf()\&.
.SS "__extern int caca_clear_canvas (\fBcaca_canvas_t\fP * cv)"
Clear the canvas using the current foreground and background colours\&.
.PP
This function never fails\&.
.PP
\fBParameters:\fP
.RS 4
\fIcv\fP The canvas to clear\&. 
.RE
.PP
\fBReturns:\fP
.RS 4
This function always returns 0\&. 
.RE
.PP

.PP
References caca_add_dirty_rect()\&.
.PP
Referenced by caca_conio_clrscr()\&.
.SS "__extern int caca_set_canvas_handle (\fBcaca_canvas_t\fP * cv, int x, int y)"
Set the canvas' handle\&. Blitting functions will use the handle value to put the canvas at the proper coordinates\&.
.PP
This function never fails\&.
.PP
\fBParameters:\fP
.RS 4
\fIcv\fP A handle to the libcaca canvas\&. 
.br
\fIx\fP X handle coordinate\&. 
.br
\fIy\fP Y handle coordinate\&. 
.RE
.PP
\fBReturns:\fP
.RS 4
This function always returns 0\&. 
.RE
.PP

.PP
Referenced by caca_put_figchar()\&.
.SS "__extern int caca_get_canvas_handle_x (\fBcaca_canvas_t\fP const * cv)"
Retrieve the X coordinate of the canvas' handle\&.
.PP
This function never fails\&.
.PP
\fBParameters:\fP
.RS 4
\fIcv\fP A handle to the libcaca canvas\&. 
.RE
.PP
\fBReturns:\fP
.RS 4
The canvas' handle's X coordinate\&. 
.RE
.PP

.SS "__extern int caca_get_canvas_handle_y (\fBcaca_canvas_t\fP const * cv)"
Retrieve the Y coordinate of the canvas' handle\&.
.PP
This function never fails\&.
.PP
\fBParameters:\fP
.RS 4
\fIcv\fP A handle to the libcaca canvas\&. 
.RE
.PP
\fBReturns:\fP
.RS 4
The canvas' handle's Y coordinate\&. 
.RE
.PP

.SS "__extern int caca_blit (\fBcaca_canvas_t\fP * dst, int x, int y, \fBcaca_canvas_t\fP const * src, \fBcaca_canvas_t\fP const * mask)"
Blit a canvas onto another one at the given coordinates\&. An optional mask canvas can be used\&.
.PP
If an error occurs, -1 is returned and \fBerrno\fP is set accordingly:
.IP "\(bu" 2
\fCEINVAL\fP A mask was specified but the mask size and source canvas size do not match\&.
.PP
.PP
\fBParameters:\fP
.RS 4
\fIdst\fP The destination canvas\&. 
.br
\fIx\fP X coordinate\&. 
.br
\fIy\fP Y coordinate\&. 
.br
\fIsrc\fP The source canvas\&. 
.br
\fImask\fP The mask canvas\&. 
.RE
.PP
\fBReturns:\fP
.RS 4
0 in case of success, -1 if an error occurred\&. 
.RE
.PP

.PP
References caca_add_dirty_rect(), and CACA_MAGIC_FULLWIDTH\&.
.PP
Referenced by caca_conio_movetext(), caca_export_area_to_memory(), caca_import_area_from_file(), caca_import_area_from_memory(), caca_put_figchar(), and caca_set_canvas_boundaries()\&.
.SS "__extern int caca_set_canvas_boundaries (\fBcaca_canvas_t\fP * cv, int x, int y, int w, int h)"
Set new boundaries for a canvas\&. This function can be used to crop a canvas, to expand it or for combinations of both actions\&. All frames are affected by this function\&.
.PP
If an error occurs, -1 is returned and \fBerrno\fP is set accordingly:
.IP "\(bu" 2
\fCEINVAL\fP Specified width or height is invalid\&.
.IP "\(bu" 2
\fCEBUSY\fP The canvas is in use by a display driver and cannot be resized\&.
.IP "\(bu" 2
\fCENOMEM\fP Not enough memory for the requested canvas size\&. If this happens, the canvas handle becomes invalid and should not be used\&.
.PP
.PP
\fBParameters:\fP
.RS 4
\fIcv\fP The canvas to crop\&. 
.br
\fIx\fP X coordinate of the top-left corner\&. 
.br
\fIy\fP Y coordinate of the top-left corner\&. 
.br
\fIw\fP The width of the cropped area\&. 
.br
\fIh\fP The height of the cropped area\&. 
.RE
.PP
\fBReturns:\fP
.RS 4
0 in case of success, -1 if an error occurred\&. 
.RE
.PP

.PP
References caca_add_dirty_rect(), caca_blit(), caca_create_canvas(), caca_create_frame(), caca_get_frame_count(), and caca_set_frame()\&.
.SH "Author"
.PP 
Generated automatically by Doxygen for libcaca from the source code\&.
