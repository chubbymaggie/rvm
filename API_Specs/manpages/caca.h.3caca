.TH "caca.h" 3caca "Sun Feb 7 2016" "Version 0.99.beta19" "libcaca" \" -*- nroff -*-
.ad l
.nh
.SH NAME
caca.h \- The \fIlibcaca\fP public header\&.  

.SH SYNOPSIS
.br
.PP
.SS "Data Structures"

.in +1c
.ti -1c
.RI "struct \fBcaca_event\fP"
.br
.RI "\fIHandling of user events\&. \fP"
.ti -1c
.RI "struct \fBcaca_option\fP"
.br
.RI "\fIOption parsing\&. \fP"
.ti -1c
.RI "struct \fBcaca_conio_text_info\fP"
.br
.RI "\fIDOS text area information\&. \fP"
.in -1c
.SS "Macros"

.in +1c
.ti -1c
.RI "#define \fBCACA_API_VERSION_1\fP"
.br
.ti -1c
.RI "#define \fBCACA_MAGIC_FULLWIDTH\fP   0x000ffffe"
.br
.in -1c
.SS "Typedefs"

.in +1c
.ti -1c
.RI "typedef struct caca_canvas \fBcaca_canvas_t\fP"
.br
.ti -1c
.RI "typedef struct caca_dither \fBcaca_dither_t\fP"
.br
.ti -1c
.RI "typedef struct caca_charfont \fBcaca_charfont_t\fP"
.br
.ti -1c
.RI "typedef struct caca_font \fBcaca_font_t\fP"
.br
.ti -1c
.RI "typedef struct caca_file \fBcaca_file_t\fP"
.br
.ti -1c
.RI "typedef struct caca_display \fBcaca_display_t\fP"
.br
.ti -1c
.RI "typedef struct \fBcaca_event\fP \fBcaca_event_t\fP"
.br
.in -1c
.SS "Enumerations"

.in +1c
.ti -1c
.RI "enum \fBcaca_color\fP { \fBCACA_BLACK\fP = 0x00, \fBCACA_BLUE\fP = 0x01, \fBCACA_GREEN\fP = 0x02, \fBCACA_CYAN\fP = 0x03, \fBCACA_RED\fP = 0x04, \fBCACA_MAGENTA\fP = 0x05, \fBCACA_BROWN\fP = 0x06, \fBCACA_LIGHTGRAY\fP = 0x07, \fBCACA_DARKGRAY\fP = 0x08, \fBCACA_LIGHTBLUE\fP = 0x09, \fBCACA_LIGHTGREEN\fP = 0x0a, \fBCACA_LIGHTCYAN\fP = 0x0b, \fBCACA_LIGHTRED\fP = 0x0c, \fBCACA_LIGHTMAGENTA\fP = 0x0d, \fBCACA_YELLOW\fP = 0x0e, \fBCACA_WHITE\fP = 0x0f, \fBCACA_DEFAULT\fP = 0x10, \fBCACA_TRANSPARENT\fP = 0x20 }"
.br
.ti -1c
.RI "enum \fBcaca_style\fP { \fBCACA_BOLD\fP = 0x01, \fBCACA_ITALICS\fP = 0x02, \fBCACA_UNDERLINE\fP = 0x04, \fBCACA_BLINK\fP = 0x08 }"
.br
.ti -1c
.RI "enum \fBcaca_event_type\fP { \fBCACA_EVENT_NONE\fP = 0x0000, \fBCACA_EVENT_KEY_PRESS\fP = 0x0001, \fBCACA_EVENT_KEY_RELEASE\fP = 0x0002, \fBCACA_EVENT_MOUSE_PRESS\fP = 0x0004, \fBCACA_EVENT_MOUSE_RELEASE\fP = 0x0008, \fBCACA_EVENT_MOUSE_MOTION\fP = 0x0010, \fBCACA_EVENT_RESIZE\fP = 0x0020, \fBCACA_EVENT_QUIT\fP = 0x0040, \fBCACA_EVENT_ANY\fP = 0xffff }"
.br
.RI "\fIUser event type enumeration\&. \fP"
.ti -1c
.RI "enum \fBcaca_key\fP { \fBCACA_KEY_UNKNOWN\fP = 0x00, \fBCACA_KEY_CTRL_A\fP = 0x01, \fBCACA_KEY_CTRL_B\fP = 0x02, \fBCACA_KEY_CTRL_C\fP = 0x03, \fBCACA_KEY_CTRL_D\fP = 0x04, \fBCACA_KEY_CTRL_E\fP = 0x05, \fBCACA_KEY_CTRL_F\fP = 0x06, \fBCACA_KEY_CTRL_G\fP = 0x07, \fBCACA_KEY_BACKSPACE\fP = 0x08, \fBCACA_KEY_TAB\fP = 0x09, \fBCACA_KEY_CTRL_J\fP = 0x0a, \fBCACA_KEY_CTRL_K\fP = 0x0b, \fBCACA_KEY_CTRL_L\fP = 0x0c, \fBCACA_KEY_RETURN\fP = 0x0d, \fBCACA_KEY_CTRL_N\fP = 0x0e, \fBCACA_KEY_CTRL_O\fP = 0x0f, \fBCACA_KEY_CTRL_P\fP = 0x10, \fBCACA_KEY_CTRL_Q\fP = 0x11, \fBCACA_KEY_CTRL_R\fP = 0x12, \fBCACA_KEY_PAUSE\fP = 0x13, \fBCACA_KEY_CTRL_T\fP = 0x14, \fBCACA_KEY_CTRL_U\fP = 0x15, \fBCACA_KEY_CTRL_V\fP = 0x16, \fBCACA_KEY_CTRL_W\fP = 0x17, \fBCACA_KEY_CTRL_X\fP = 0x18, \fBCACA_KEY_CTRL_Y\fP = 0x19, \fBCACA_KEY_CTRL_Z\fP = 0x1a, \fBCACA_KEY_ESCAPE\fP = 0x1b, \fBCACA_KEY_DELETE\fP = 0x7f, \fBCACA_KEY_UP\fP = 0x111, \fBCACA_KEY_DOWN\fP = 0x112, \fBCACA_KEY_LEFT\fP = 0x113, \fBCACA_KEY_RIGHT\fP = 0x114, \fBCACA_KEY_INSERT\fP = 0x115, \fBCACA_KEY_HOME\fP = 0x116, \fBCACA_KEY_END\fP = 0x117, \fBCACA_KEY_PAGEUP\fP = 0x118, \fBCACA_KEY_PAGEDOWN\fP = 0x119, \fBCACA_KEY_F1\fP = 0x11a, \fBCACA_KEY_F2\fP = 0x11b, \fBCACA_KEY_F3\fP = 0x11c, \fBCACA_KEY_F4\fP = 0x11d, \fBCACA_KEY_F5\fP = 0x11e, \fBCACA_KEY_F6\fP = 0x11f, \fBCACA_KEY_F7\fP = 0x120, \fBCACA_KEY_F8\fP = 0x121, \fBCACA_KEY_F9\fP = 0x122, \fBCACA_KEY_F10\fP = 0x123, \fBCACA_KEY_F11\fP = 0x124, \fBCACA_KEY_F12\fP = 0x125, \fBCACA_KEY_F13\fP = 0x126, \fBCACA_KEY_F14\fP = 0x127, \fBCACA_KEY_F15\fP = 0x128 }"
.br
.RI "\fISpecial key values\&. \fP"
.ti -1c
.RI "enum \fBCACA_CONIO_COLORS\fP { \fBCACA_CONIO_BLINK\fP = 128, \fBCACA_CONIO_BLACK\fP = 0, \fBCACA_CONIO_BLUE\fP = 1, \fBCACA_CONIO_GREEN\fP = 2, \fBCACA_CONIO_CYAN\fP = 3, \fBCACA_CONIO_RED\fP = 4, \fBCACA_CONIO_MAGENTA\fP = 5, \fBCACA_CONIO_BROWN\fP = 6, \fBCACA_CONIO_LIGHTGRAY\fP = 7, \fBCACA_CONIO_DARKGRAY\fP = 8, \fBCACA_CONIO_LIGHTBLUE\fP = 9, \fBCACA_CONIO_LIGHTGREEN\fP = 10, \fBCACA_CONIO_LIGHTCYAN\fP = 11, \fBCACA_CONIO_LIGHTRED\fP = 12, \fBCACA_CONIO_LIGHTMAGENTA\fP = 13, \fBCACA_CONIO_YELLOW\fP = 14, \fBCACA_CONIO_WHITE\fP = 15 }"
.br
.RI "\fIDOS colours\&. \fP"
.ti -1c
.RI "enum \fBCACA_CONIO_CURSOR\fP { \fBCACA_CONIO__NOCURSOR\fP = 0, \fBCACA_CONIO__SOLIDCURSOR\fP = 1, \fBCACA_CONIO__NORMALCURSOR\fP = 2 }"
.br
.RI "\fIDOS cursor modes\&. \fP"
.ti -1c
.RI "enum \fBCACA_CONIO_MODE\fP { \fBCACA_CONIO_LASTMODE\fP = -1, \fBCACA_CONIO_BW40\fP = 0, \fBCACA_CONIO_C40\fP = 1, \fBCACA_CONIO_BW80\fP = 2, \fBCACA_CONIO_C80\fP = 3, \fBCACA_CONIO_MONO\fP = 7, \fBCACA_CONIO_C4350\fP = 64 }"
.br
.RI "\fIDOS video modes\&. \fP"
.in -1c
.SS "Functions"

.in +1c
.ti -1c
.RI "__extern \fBcaca_canvas_t\fP * \fBcaca_create_canvas\fP (int, int)"
.br
.RI "\fIInitialise a \fIlibcaca\fP canvas\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_manage_canvas\fP (\fBcaca_canvas_t\fP *, int(*)(void *), void *)"
.br
.RI "\fIManage a canvas\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_unmanage_canvas\fP (\fBcaca_canvas_t\fP *, int(*)(void *), void *)"
.br
.RI "\fIunmanage a canvas\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_set_canvas_size\fP (\fBcaca_canvas_t\fP *, int, int)"
.br
.RI "\fIResize a canvas\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_get_canvas_width\fP (\fBcaca_canvas_t\fP const *)"
.br
.RI "\fIGet the canvas width\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_get_canvas_height\fP (\fBcaca_canvas_t\fP const *)"
.br
.RI "\fIGet the canvas height\&. \fP"
.ti -1c
.RI "__extern uint32_t const * \fBcaca_get_canvas_chars\fP (\fBcaca_canvas_t\fP const *)"
.br
.RI "\fIGet the canvas character array\&. \fP"
.ti -1c
.RI "__extern uint32_t const * \fBcaca_get_canvas_attrs\fP (\fBcaca_canvas_t\fP const *)"
.br
.RI "\fIGet the canvas attribute array\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_free_canvas\fP (\fBcaca_canvas_t\fP *)"
.br
.RI "\fIFree a \fIlibcaca\fP canvas\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_rand\fP (int, int)"
.br
.ti -1c
.RI "__extern char const * \fBcaca_get_version\fP (void)"
.br
.RI "\fIReturn the \fIlibcaca\fP version\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_gotoxy\fP (\fBcaca_canvas_t\fP *, int, int)"
.br
.RI "\fISet cursor position\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_wherex\fP (\fBcaca_canvas_t\fP const *)"
.br
.RI "\fIGet X cursor position\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_wherey\fP (\fBcaca_canvas_t\fP const *)"
.br
.RI "\fIGet Y cursor position\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_put_char\fP (\fBcaca_canvas_t\fP *, int, int, uint32_t)"
.br
.RI "\fIPrint an ASCII or Unicode character\&. \fP"
.ti -1c
.RI "__extern uint32_t \fBcaca_get_char\fP (\fBcaca_canvas_t\fP const *, int, int)"
.br
.RI "\fIGet the Unicode character at the given coordinates\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_put_str\fP (\fBcaca_canvas_t\fP *, int, int, char const *)"
.br
.RI "\fIPrint a string\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_printf\fP (\fBcaca_canvas_t\fP *, int, int, char const *,\&.\&.\&.)"
.br
.RI "\fIPrint a formated string\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_vprintf\fP (\fBcaca_canvas_t\fP *, int, int, char const *, va_list)"
.br
.RI "\fIPrint a formated string (va_list version)\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_clear_canvas\fP (\fBcaca_canvas_t\fP *)"
.br
.RI "\fIClear the canvas\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_set_canvas_handle\fP (\fBcaca_canvas_t\fP *, int, int)"
.br
.RI "\fISet cursor handle\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_get_canvas_handle_x\fP (\fBcaca_canvas_t\fP const *)"
.br
.RI "\fIGet X handle position\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_get_canvas_handle_y\fP (\fBcaca_canvas_t\fP const *)"
.br
.RI "\fIGet Y handle position\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_blit\fP (\fBcaca_canvas_t\fP *, int, int, \fBcaca_canvas_t\fP const *, \fBcaca_canvas_t\fP const *)"
.br
.RI "\fIBlit a canvas onto another one\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_set_canvas_boundaries\fP (\fBcaca_canvas_t\fP *, int, int, int, int)"
.br
.RI "\fISet a canvas' new boundaries\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_disable_dirty_rect\fP (\fBcaca_canvas_t\fP *)"
.br
.RI "\fIDisable dirty rectangles\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_enable_dirty_rect\fP (\fBcaca_canvas_t\fP *)"
.br
.RI "\fIEnable dirty rectangles\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_get_dirty_rect_count\fP (\fBcaca_canvas_t\fP *)"
.br
.RI "\fIGet the number of dirty rectangles in the canvas\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_get_dirty_rect\fP (\fBcaca_canvas_t\fP *, int, int *, int *, int *, int *)"
.br
.RI "\fIGet a canvas's dirty rectangle\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_add_dirty_rect\fP (\fBcaca_canvas_t\fP *, int, int, int, int)"
.br
.RI "\fIAdd an area to the canvas's dirty rectangle list\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_remove_dirty_rect\fP (\fBcaca_canvas_t\fP *, int, int, int, int)"
.br
.RI "\fIRemove an area from the dirty rectangle list\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_clear_dirty_rect_list\fP (\fBcaca_canvas_t\fP *)"
.br
.RI "\fIClear a canvas's dirty rectangle list\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_invert\fP (\fBcaca_canvas_t\fP *)"
.br
.RI "\fIInvert a canvas' colours\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_flip\fP (\fBcaca_canvas_t\fP *)"
.br
.RI "\fIFlip a canvas horizontally\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_flop\fP (\fBcaca_canvas_t\fP *)"
.br
.RI "\fIFlip a canvas vertically\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_rotate_180\fP (\fBcaca_canvas_t\fP *)"
.br
.RI "\fIRotate a canvas\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_rotate_left\fP (\fBcaca_canvas_t\fP *)"
.br
.RI "\fIRotate a canvas, 90 degrees counterclockwise\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_rotate_right\fP (\fBcaca_canvas_t\fP *)"
.br
.RI "\fIRotate a canvas, 90 degrees counterclockwise\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_stretch_left\fP (\fBcaca_canvas_t\fP *)"
.br
.RI "\fIRotate and stretch a canvas, 90 degrees counterclockwise\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_stretch_right\fP (\fBcaca_canvas_t\fP *)"
.br
.RI "\fIRotate and stretch a canvas, 90 degrees clockwise\&. \fP"
.ti -1c
.RI "__extern uint32_t \fBcaca_get_attr\fP (\fBcaca_canvas_t\fP const *, int, int)"
.br
.RI "\fIGet the text attribute at the given coordinates\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_set_attr\fP (\fBcaca_canvas_t\fP *, uint32_t)"
.br
.RI "\fISet the default character attribute\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_unset_attr\fP (\fBcaca_canvas_t\fP *, uint32_t)"
.br
.RI "\fIUnset flags in the default character attribute\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_toggle_attr\fP (\fBcaca_canvas_t\fP *, uint32_t)"
.br
.RI "\fIToggle flags in the default character attribute\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_put_attr\fP (\fBcaca_canvas_t\fP *, int, int, uint32_t)"
.br
.RI "\fISet the character attribute at the given coordinates\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_set_color_ansi\fP (\fBcaca_canvas_t\fP *, uint8_t, uint8_t)"
.br
.RI "\fISet the default colour pair for text (ANSI version)\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_set_color_argb\fP (\fBcaca_canvas_t\fP *, uint16_t, uint16_t)"
.br
.RI "\fISet the default colour pair for text (truecolor version)\&. \fP"
.ti -1c
.RI "__extern uint8_t \fBcaca_attr_to_ansi\fP (uint32_t)"
.br
.RI "\fIGet DOS ANSI information from attribute\&. \fP"
.ti -1c
.RI "__extern uint8_t \fBcaca_attr_to_ansi_fg\fP (uint32_t)"
.br
.RI "\fIGet ANSI foreground information from attribute\&. \fP"
.ti -1c
.RI "__extern uint8_t \fBcaca_attr_to_ansi_bg\fP (uint32_t)"
.br
.RI "\fIGet ANSI background information from attribute\&. \fP"
.ti -1c
.RI "__extern uint16_t \fBcaca_attr_to_rgb12_fg\fP (uint32_t)"
.br
.RI "\fIGet 12-bit RGB foreground information from attribute\&. \fP"
.ti -1c
.RI "__extern uint16_t \fBcaca_attr_to_rgb12_bg\fP (uint32_t)"
.br
.RI "\fIGet 12-bit RGB background information from attribute\&. \fP"
.ti -1c
.RI "__extern void \fBcaca_attr_to_argb64\fP (uint32_t, uint8_t[8])"
.br
.RI "\fIGet 64-bit ARGB information from attribute\&. \fP"
.ti -1c
.RI "__extern uint32_t \fBcaca_utf8_to_utf32\fP (char const *, size_t *)"
.br
.RI "\fIConvert a UTF-8 character to UTF-32\&. \fP"
.ti -1c
.RI "__extern size_t \fBcaca_utf32_to_utf8\fP (char *, uint32_t)"
.br
.RI "\fIConvert a UTF-32 character to UTF-8\&. \fP"
.ti -1c
.RI "__extern uint8_t \fBcaca_utf32_to_cp437\fP (uint32_t)"
.br
.RI "\fIConvert a UTF-32 character to CP437\&. \fP"
.ti -1c
.RI "__extern uint32_t \fBcaca_cp437_to_utf32\fP (uint8_t)"
.br
.RI "\fIConvert a CP437 character to UTF-32\&. \fP"
.ti -1c
.RI "__extern char \fBcaca_utf32_to_ascii\fP (uint32_t)"
.br
.RI "\fIConvert a UTF-32 character to ASCII\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_utf32_is_fullwidth\fP (uint32_t)"
.br
.RI "\fITell whether a UTF-32 character is fullwidth\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_draw_line\fP (\fBcaca_canvas_t\fP *, int, int, int, int, uint32_t)"
.br
.RI "\fIDraw a line on the canvas using the given character\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_draw_polyline\fP (\fBcaca_canvas_t\fP *, int const x[], int const y[], int, uint32_t)"
.br
.RI "\fIDraw a polyline\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_draw_thin_line\fP (\fBcaca_canvas_t\fP *, int, int, int, int)"
.br
.RI "\fIDraw a thin line on the canvas, using ASCII art\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_draw_thin_polyline\fP (\fBcaca_canvas_t\fP *, int const x[], int const y[], int)"
.br
.RI "\fIDraw an ASCII art thin polyline\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_draw_circle\fP (\fBcaca_canvas_t\fP *, int, int, int, uint32_t)"
.br
.RI "\fIDraw a circle on the canvas using the given character\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_draw_ellipse\fP (\fBcaca_canvas_t\fP *, int, int, int, int, uint32_t)"
.br
.RI "\fIDraw an ellipse on the canvas using the given character\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_draw_thin_ellipse\fP (\fBcaca_canvas_t\fP *, int, int, int, int)"
.br
.RI "\fIDraw a thin ellipse on the canvas\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_fill_ellipse\fP (\fBcaca_canvas_t\fP *, int, int, int, int, uint32_t)"
.br
.RI "\fIFill an ellipse on the canvas using the given character\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_draw_box\fP (\fBcaca_canvas_t\fP *, int, int, int, int, uint32_t)"
.br
.RI "\fIDraw a box on the canvas using the given character\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_draw_thin_box\fP (\fBcaca_canvas_t\fP *, int, int, int, int)"
.br
.RI "\fIDraw a thin box on the canvas\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_draw_cp437_box\fP (\fBcaca_canvas_t\fP *, int, int, int, int)"
.br
.RI "\fIDraw a box on the canvas using CP437 characters\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_fill_box\fP (\fBcaca_canvas_t\fP *, int, int, int, int, uint32_t)"
.br
.RI "\fIFill a box on the canvas using the given character\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_draw_triangle\fP (\fBcaca_canvas_t\fP *, int, int, int, int, int, int, uint32_t)"
.br
.RI "\fIDraw a triangle on the canvas using the given character\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_draw_thin_triangle\fP (\fBcaca_canvas_t\fP *, int, int, int, int, int, int)"
.br
.RI "\fIDraw a thin triangle on the canvas\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_fill_triangle\fP (\fBcaca_canvas_t\fP *, int, int, int, int, int, int, uint32_t)"
.br
.RI "\fIFill a triangle on the canvas using the given character\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_fill_triangle_textured\fP (\fBcaca_canvas_t\fP *cv, int coords[6], \fBcaca_canvas_t\fP *tex, float uv[6])"
.br
.RI "\fIFill a triangle on the canvas using an arbitrary-sized texture\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_get_frame_count\fP (\fBcaca_canvas_t\fP const *)"
.br
.RI "\fIGet the number of frames in a canvas\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_set_frame\fP (\fBcaca_canvas_t\fP *, int)"
.br
.RI "\fIActivate a given canvas frame\&. \fP"
.ti -1c
.RI "__extern char const * \fBcaca_get_frame_name\fP (\fBcaca_canvas_t\fP const *)"
.br
.RI "\fIGet the current frame's name\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_set_frame_name\fP (\fBcaca_canvas_t\fP *, char const *)"
.br
.RI "\fISet the current frame's name\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_create_frame\fP (\fBcaca_canvas_t\fP *, int)"
.br
.RI "\fIAdd a frame to a canvas\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_free_frame\fP (\fBcaca_canvas_t\fP *, int)"
.br
.RI "\fIRemove a frame from a canvas\&. \fP"
.ti -1c
.RI "__extern \fBcaca_dither_t\fP * \fBcaca_create_dither\fP (int, int, int, int, uint32_t, uint32_t, uint32_t, uint32_t)"
.br
.RI "\fICreate an internal dither object\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_set_dither_palette\fP (\fBcaca_dither_t\fP *, uint32_t r[], uint32_t g[], uint32_t b[], uint32_t a[])"
.br
.RI "\fISet the palette of an 8bpp dither object\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_set_dither_brightness\fP (\fBcaca_dither_t\fP *, float)"
.br
.RI "\fISet the brightness of a dither object\&. \fP"
.ti -1c
.RI "__extern float \fBcaca_get_dither_brightness\fP (\fBcaca_dither_t\fP const *)"
.br
.RI "\fIGet the brightness of a dither object\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_set_dither_gamma\fP (\fBcaca_dither_t\fP *, float)"
.br
.RI "\fISet the gamma of a dither object\&. \fP"
.ti -1c
.RI "__extern float \fBcaca_get_dither_gamma\fP (\fBcaca_dither_t\fP const *)"
.br
.RI "\fIGet the gamma of a dither object\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_set_dither_contrast\fP (\fBcaca_dither_t\fP *, float)"
.br
.RI "\fISet the contrast of a dither object\&. \fP"
.ti -1c
.RI "__extern float \fBcaca_get_dither_contrast\fP (\fBcaca_dither_t\fP const *)"
.br
.RI "\fIGet the contrast of a dither object\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_set_dither_antialias\fP (\fBcaca_dither_t\fP *, char const *)"
.br
.RI "\fISet dither antialiasing\&. \fP"
.ti -1c
.RI "__extern char const *const * \fBcaca_get_dither_antialias_list\fP (\fBcaca_dither_t\fP const *)"
.br
.RI "\fIGet available antialiasing methods\&. \fP"
.ti -1c
.RI "__extern char const * \fBcaca_get_dither_antialias\fP (\fBcaca_dither_t\fP const *)"
.br
.RI "\fIGet current antialiasing method\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_set_dither_color\fP (\fBcaca_dither_t\fP *, char const *)"
.br
.RI "\fIChoose colours used for dithering\&. \fP"
.ti -1c
.RI "__extern char const *const * \fBcaca_get_dither_color_list\fP (\fBcaca_dither_t\fP const *)"
.br
.RI "\fIGet available colour modes\&. \fP"
.ti -1c
.RI "__extern char const * \fBcaca_get_dither_color\fP (\fBcaca_dither_t\fP const *)"
.br
.RI "\fIGet current colour mode\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_set_dither_charset\fP (\fBcaca_dither_t\fP *, char const *)"
.br
.RI "\fIChoose characters used for dithering\&. \fP"
.ti -1c
.RI "__extern char const *const * \fBcaca_get_dither_charset_list\fP (\fBcaca_dither_t\fP const *)"
.br
.RI "\fIGet available dither character sets\&. \fP"
.ti -1c
.RI "__extern char const * \fBcaca_get_dither_charset\fP (\fBcaca_dither_t\fP const *)"
.br
.RI "\fIGet current character set\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_set_dither_algorithm\fP (\fBcaca_dither_t\fP *, char const *)"
.br
.RI "\fISet dithering algorithm\&. \fP"
.ti -1c
.RI "__extern char const *const * \fBcaca_get_dither_algorithm_list\fP (\fBcaca_dither_t\fP const *)"
.br
.RI "\fIGet dithering algorithms\&. \fP"
.ti -1c
.RI "__extern char const * \fBcaca_get_dither_algorithm\fP (\fBcaca_dither_t\fP const *)"
.br
.RI "\fIGet current dithering algorithm\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_dither_bitmap\fP (\fBcaca_canvas_t\fP *, int, int, int, int, \fBcaca_dither_t\fP const *, void const *)"
.br
.RI "\fIDither a bitmap on the canvas\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_free_dither\fP (\fBcaca_dither_t\fP *)"
.br
.RI "\fIFree the memory associated with a dither\&. \fP"
.ti -1c
.RI "__extern \fBcaca_charfont_t\fP * \fBcaca_load_charfont\fP (void const *, size_t)"
.br
.ti -1c
.RI "__extern int \fBcaca_free_charfont\fP (\fBcaca_charfont_t\fP *)"
.br
.ti -1c
.RI "__extern \fBcaca_font_t\fP * \fBcaca_load_font\fP (void const *, size_t)"
.br
.RI "\fILoad a font from memory for future use\&. \fP"
.ti -1c
.RI "__extern char const *const * \fBcaca_get_font_list\fP (void)"
.br
.RI "\fIGet available builtin fonts\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_get_font_width\fP (\fBcaca_font_t\fP const *)"
.br
.RI "\fIGet a font's standard glyph width\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_get_font_height\fP (\fBcaca_font_t\fP const *)"
.br
.RI "\fIGet a font's standard glyph height\&. \fP"
.ti -1c
.RI "__extern uint32_t const * \fBcaca_get_font_blocks\fP (\fBcaca_font_t\fP const *)"
.br
.RI "\fIGet a font's list of supported glyphs\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_render_canvas\fP (\fBcaca_canvas_t\fP const *, \fBcaca_font_t\fP const *, void *, int, int, int)"
.br
.RI "\fIRender the canvas onto an image buffer\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_free_font\fP (\fBcaca_font_t\fP *)"
.br
.RI "\fIFree a font structure\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_canvas_set_figfont\fP (\fBcaca_canvas_t\fP *, char const *)"
.br
.RI "\fIload a figfont and attach it to a canvas \fP"
.ti -1c
.RI "__extern int \fBcaca_set_figfont_smush\fP (\fBcaca_canvas_t\fP *, char const *)"
.br
.RI "\fIset the smushing mode of the figfont rendering \fP"
.ti -1c
.RI "__extern int \fBcaca_set_figfont_width\fP (\fBcaca_canvas_t\fP *, int)"
.br
.RI "\fIset the width of the figfont rendering \fP"
.ti -1c
.RI "__extern int \fBcaca_put_figchar\fP (\fBcaca_canvas_t\fP *, uint32_t)"
.br
.RI "\fIpaste a character using the current figfont \fP"
.ti -1c
.RI "__extern int \fBcaca_flush_figlet\fP (\fBcaca_canvas_t\fP *)"
.br
.RI "\fIflush the figlet context \fP"
.ti -1c
.RI "__extern \fBcaca_file_t\fP * \fBcaca_file_open\fP (char const *, const char *)"
.br
.RI "\fIOpen a file for reading or writing\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_file_close\fP (\fBcaca_file_t\fP *)"
.br
.RI "\fIClose a file handle\&. \fP"
.ti -1c
.RI "__extern uint64_t \fBcaca_file_tell\fP (\fBcaca_file_t\fP *)"
.br
.RI "\fIReturn the position in a file handle\&. \fP"
.ti -1c
.RI "__extern size_t \fBcaca_file_read\fP (\fBcaca_file_t\fP *, void *, size_t)"
.br
.RI "\fIRead data from a file handle\&. \fP"
.ti -1c
.RI "__extern size_t \fBcaca_file_write\fP (\fBcaca_file_t\fP *, const void *, size_t)"
.br
.RI "\fIWrite data to a file handle\&. \fP"
.ti -1c
.RI "__extern char * \fBcaca_file_gets\fP (\fBcaca_file_t\fP *, char *, int)"
.br
.RI "\fIRead a line from a file handle\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_file_eof\fP (\fBcaca_file_t\fP *)"
.br
.RI "\fITell whether a file handle reached end of file\&. \fP"
.ti -1c
.RI "__extern ssize_t \fBcaca_import_canvas_from_memory\fP (\fBcaca_canvas_t\fP *, void const *, size_t, char const *)"
.br
.RI "\fIImport a memory buffer into a canvas\&. \fP"
.ti -1c
.RI "__extern ssize_t \fBcaca_import_canvas_from_file\fP (\fBcaca_canvas_t\fP *, char const *, char const *)"
.br
.RI "\fIImport a file into a canvas\&. \fP"
.ti -1c
.RI "__extern ssize_t \fBcaca_import_area_from_memory\fP (\fBcaca_canvas_t\fP *, int, int, void const *, size_t, char const *)"
.br
.RI "\fIImport a memory buffer into a canvas area\&. \fP"
.ti -1c
.RI "__extern ssize_t \fBcaca_import_area_from_file\fP (\fBcaca_canvas_t\fP *, int, int, char const *, char const *)"
.br
.RI "\fIImport a file into a canvas area\&. \fP"
.ti -1c
.RI "__extern char const *const * \fBcaca_get_import_list\fP (void)"
.br
.RI "\fIGet available import formats\&. \fP"
.ti -1c
.RI "__extern void * \fBcaca_export_canvas_to_memory\fP (\fBcaca_canvas_t\fP const *, char const *, size_t *)"
.br
.RI "\fIExport a canvas into a foreign format\&. \fP"
.ti -1c
.RI "__extern void * \fBcaca_export_area_to_memory\fP (\fBcaca_canvas_t\fP const *, int, int, int, int, char const *, size_t *)"
.br
.RI "\fIExport a canvas portion into a foreign format\&. \fP"
.ti -1c
.RI "__extern char const *const * \fBcaca_get_export_list\fP (void)"
.br
.RI "\fIGet available export formats\&. \fP"
.ti -1c
.RI "__extern \fBcaca_display_t\fP * \fBcaca_create_display\fP (\fBcaca_canvas_t\fP *)"
.br
.RI "\fIAttach a caca graphical context to a caca canvas\&. \fP"
.ti -1c
.RI "__extern \fBcaca_display_t\fP * \fBcaca_create_display_with_driver\fP (\fBcaca_canvas_t\fP *, char const *)"
.br
.RI "\fIAttach a specific caca graphical context to a caca canvas\&. \fP"
.ti -1c
.RI "__extern char const *const * \fBcaca_get_display_driver_list\fP (void)"
.br
.RI "\fIGet available display drivers\&. \fP"
.ti -1c
.RI "__extern char const * \fBcaca_get_display_driver\fP (\fBcaca_display_t\fP *)"
.br
.RI "\fIReturn a caca graphical context's current output driver\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_set_display_driver\fP (\fBcaca_display_t\fP *, char const *)"
.br
.RI "\fISet the output driver\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_free_display\fP (\fBcaca_display_t\fP *)"
.br
.RI "\fIDetach a caca graphical context from a caca backend context\&. \fP"
.ti -1c
.RI "__extern \fBcaca_canvas_t\fP * \fBcaca_get_canvas\fP (\fBcaca_display_t\fP *)"
.br
.RI "\fIGet the canvas attached to a caca graphical context\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_refresh_display\fP (\fBcaca_display_t\fP *)"
.br
.RI "\fIFlush pending changes and redraw the screen\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_set_display_time\fP (\fBcaca_display_t\fP *, int)"
.br
.RI "\fISet the refresh delay\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_get_display_time\fP (\fBcaca_display_t\fP const *)"
.br
.RI "\fIGet the display's average rendering time\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_get_display_width\fP (\fBcaca_display_t\fP const *)"
.br
.RI "\fIGet the display width\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_get_display_height\fP (\fBcaca_display_t\fP const *)"
.br
.RI "\fIGet the display height\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_set_display_title\fP (\fBcaca_display_t\fP *, char const *)"
.br
.RI "\fISet the display title\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_set_mouse\fP (\fBcaca_display_t\fP *, int)"
.br
.RI "\fIShow or hide the mouse pointer\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_set_cursor\fP (\fBcaca_display_t\fP *, int)"
.br
.RI "\fIShow or hide the cursor\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_get_event\fP (\fBcaca_display_t\fP *, int, \fBcaca_event_t\fP *, int)"
.br
.RI "\fIGet the next mouse or keyboard input event\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_get_mouse_x\fP (\fBcaca_display_t\fP const *)"
.br
.RI "\fIReturn the X mouse coordinate\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_get_mouse_y\fP (\fBcaca_display_t\fP const *)"
.br
.RI "\fIReturn the Y mouse coordinate\&. \fP"
.ti -1c
.RI "__extern enum \fBcaca_event_type\fP \fBcaca_get_event_type\fP (\fBcaca_event_t\fP const *)"
.br
.RI "\fIReturn an event's type\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_get_event_key_ch\fP (\fBcaca_event_t\fP const *)"
.br
.RI "\fIReturn a key press or key release event's value\&. \fP"
.ti -1c
.RI "__extern uint32_t \fBcaca_get_event_key_utf32\fP (\fBcaca_event_t\fP const *)"
.br
.RI "\fIReturn a key press or key release event's Unicode value\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_get_event_key_utf8\fP (\fBcaca_event_t\fP const *, char *)"
.br
.RI "\fIReturn a key press or key release event's UTF-8 value\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_get_event_mouse_button\fP (\fBcaca_event_t\fP const *)"
.br
.RI "\fIReturn a mouse press or mouse release event's button\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_get_event_mouse_x\fP (\fBcaca_event_t\fP const *)"
.br
.RI "\fIReturn a mouse motion event's X coordinate\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_get_event_mouse_y\fP (\fBcaca_event_t\fP const *)"
.br
.RI "\fIReturn a mouse motion event's Y coordinate\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_get_event_resize_width\fP (\fBcaca_event_t\fP const *)"
.br
.RI "\fIReturn a resize event's display width value\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_get_event_resize_height\fP (\fBcaca_event_t\fP const *)"
.br
.RI "\fIReturn a resize event's display height value\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_getopt\fP (int, char *const [], char const *, struct \fBcaca_option\fP const *, int *)"
.br
.ti -1c
.RI "__extern char * \fBcaca_conio_cgets\fP (char *str)"
.br
.RI "\fIDOS conio\&.h cgets() equivalent\&. \fP"
.ti -1c
.RI "__extern void \fBcaca_conio_clreol\fP (void)"
.br
.RI "\fIDOS conio\&.h clreol() equivalent\&. \fP"
.ti -1c
.RI "__extern void \fBcaca_conio_clrscr\fP (void)"
.br
.RI "\fIDOS conio\&.h clrscr() equivalent\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_conio_cprintf\fP (const char *format,\&.\&.\&.)"
.br
.RI "\fIDOS conio\&.h cprintf() equivalent\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_conio_cputs\fP (const char *str)"
.br
.RI "\fIDOS conio\&.h cputs() equivalent\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_conio_cscanf\fP (char *format,\&.\&.\&.)"
.br
.RI "\fIDOS stdio\&.h cscanf() equivalent\&. \fP"
.ti -1c
.RI "__extern void \fBcaca_conio_delay\fP (unsigned int)"
.br
.RI "\fIDOS dos\&.h delay() equivalent\&. \fP"
.ti -1c
.RI "__extern void \fBcaca_conio_delline\fP (void)"
.br
.RI "\fIDOS conio\&.h delline() equivalent\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_conio_getch\fP (void)"
.br
.RI "\fIDOS conio\&.h getch() equivalent\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_conio_getche\fP (void)"
.br
.RI "\fIDOS conio\&.h getche() equivalent\&. \fP"
.ti -1c
.RI "__extern char * \fBcaca_conio_getpass\fP (const char *prompt)"
.br
.RI "\fIDOS conio\&.h getpass() equivalent\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_conio_gettext\fP (int left, int top, int right, int bottom, void *destin)"
.br
.RI "\fIDOS conio\&.h gettext() equivalent\&. \fP"
.ti -1c
.RI "__extern void \fBcaca_conio_gettextinfo\fP (struct \fBcaca_conio_text_info\fP *r)"
.br
.RI "\fIDOS conio\&.h gettextinfo() equivalent\&. \fP"
.ti -1c
.RI "__extern void \fBcaca_conio_gotoxy\fP (int x, int y)"
.br
.RI "\fIDOS conio\&.h gotoxy() equivalent\&. \fP"
.ti -1c
.RI "__extern void \fBcaca_conio_highvideo\fP (void)"
.br
.RI "\fIDOS conio\&.h highvideo() equivalent\&. \fP"
.ti -1c
.RI "__extern void \fBcaca_conio_insline\fP (void)"
.br
.RI "\fIDOS conio\&.h insline() equivalent\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_conio_kbhit\fP (void)"
.br
.RI "\fIDOS conio\&.h kbhit() equivalent\&. \fP"
.ti -1c
.RI "__extern void \fBcaca_conio_lowvideo\fP (void)"
.br
.RI "\fIDOS conio\&.h lowvideo() equivalent\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_conio_movetext\fP (int left, int top, int right, int bottom, int destleft, int desttop)"
.br
.RI "\fIDOS conio\&.h movetext() equivalent\&. \fP"
.ti -1c
.RI "__extern void \fBcaca_conio_normvideo\fP (void)"
.br
.RI "\fIDOS conio\&.h normvideo() equivalent\&. \fP"
.ti -1c
.RI "__extern void \fBcaca_conio_nosound\fP (void)"
.br
.RI "\fIDOS dos\&.h nosound() equivalent\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_conio_printf\fP (const char *format,\&.\&.\&.)"
.br
.RI "\fIDOS stdio\&.h printf() equivalent\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_conio_putch\fP (int ch)"
.br
.RI "\fIDOS conio\&.h putch() equivalent\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_conio_puttext\fP (int left, int top, int right, int bottom, void *destin)"
.br
.RI "\fIDOS conio\&.h puttext() equivalent\&. \fP"
.ti -1c
.RI "__extern void \fBcaca_conio__setcursortype\fP (int cur_t)"
.br
.RI "\fIDOS conio\&.h _setcursortype() equivalent\&. \fP"
.ti -1c
.RI "__extern void \fBcaca_conio_sleep\fP (unsigned int)"
.br
.RI "\fIDOS dos\&.h sleep() equivalent\&. \fP"
.ti -1c
.RI "__extern void \fBcaca_conio_sound\fP (unsigned int)"
.br
.RI "\fIDOS dos\&.h sound() equivalent\&. \fP"
.ti -1c
.RI "__extern void \fBcaca_conio_textattr\fP (int newattr)"
.br
.RI "\fIDOS conio\&.h textattr() equivalent\&. \fP"
.ti -1c
.RI "__extern void \fBcaca_conio_textbackground\fP (int newcolor)"
.br
.RI "\fIDOS conio\&.h textbackground() equivalent\&. \fP"
.ti -1c
.RI "__extern void \fBcaca_conio_textcolor\fP (int newcolor)"
.br
.RI "\fIDOS conio\&.h textcolor() equivalent\&. \fP"
.ti -1c
.RI "__extern void \fBcaca_conio_textmode\fP (int newmode)"
.br
.RI "\fIDOS conio\&.h textmode() equivalent\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_conio_ungetch\fP (int ch)"
.br
.RI "\fIDOS conio\&.h ungetch() equivalent\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_conio_wherex\fP (void)"
.br
.RI "\fIDOS conio\&.h wherex() equivalent\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_conio_wherey\fP (void)"
.br
.RI "\fIDOS conio\&.h wherey() equivalent\&. \fP"
.ti -1c
.RI "__extern void \fBcaca_conio_window\fP (int left, int top, int right, int bottom)"
.br
.RI "\fIDOS conio\&.h window() equivalent\&. \fP"
.in -1c
.SS "Variables"

.in +1c
.ti -1c
.RI "__extern int \fBcaca_optind\fP"
.br
.ti -1c
.RI "__extern char * \fBcaca_optarg\fP"
.br
.ti -1c
.RI "__extern int \fBcaca_conio_directvideo\fP"
.br
.RI "\fIDOS direct video control\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_conio__wscroll\fP"
.br
.RI "\fIDOS scrolling control\&. \fP"
.in -1c
.SH "Detailed Description"
.PP 

.PP
\fBAuthor:\fP
.RS 4
Sam Hocevar sam@hocevar.net This header contains the public types and functions that applications using \fIlibcaca\fP may use\&. 
.RE
.PP

.SH "Macro Definition Documentation"
.PP 
.SS "#define CACA_API_VERSION_1"
libcaca API version 
.SH "Typedef Documentation"
.PP 
.SS "typedef struct caca_canvas \fBcaca_canvas_t\fP"
\fIlibcaca\fP canvas 
.SS "typedef struct caca_dither \fBcaca_dither_t\fP"
dither structure 
.SS "typedef struct caca_charfont \fBcaca_charfont_t\fP"
character font structure 
.SS "typedef struct caca_font \fBcaca_font_t\fP"
bitmap font structure 
.SS "typedef struct caca_file \fBcaca_file_t\fP"
file handle structure 
.SS "typedef struct caca_display \fBcaca_display_t\fP"
\fIlibcaca\fP display context 
.SS "typedef struct \fBcaca_event\fP \fBcaca_event_t\fP"
\fIlibcaca\fP event structure 
.SH "Enumeration Type Documentation"
.PP 
.SS "enum \fBcaca_event_type\fP"
This enum serves two purposes:
.IP "\(bu" 2
Build listening masks for \fBcaca_get_event()\fP\&.
.IP "\(bu" 2
Define the type of a \fIcaca_event_t\fP\&. 
.PP

.PP
\fBEnumerator\fP
.in +1c
.TP
\fB\fICACA_EVENT_NONE \fP\fP
No event\&. 
.TP
\fB\fICACA_EVENT_KEY_PRESS \fP\fP
A key was pressed\&. 
.TP
\fB\fICACA_EVENT_KEY_RELEASE \fP\fP
A key was released\&. 
.TP
\fB\fICACA_EVENT_MOUSE_PRESS \fP\fP
A mouse button was pressed\&. 
.TP
\fB\fICACA_EVENT_MOUSE_RELEASE \fP\fP
A mouse button was released\&. 
.TP
\fB\fICACA_EVENT_MOUSE_MOTION \fP\fP
The mouse was moved\&. 
.TP
\fB\fICACA_EVENT_RESIZE \fP\fP
The window was resized\&. 
.TP
\fB\fICACA_EVENT_QUIT \fP\fP
The user requested to quit\&. 
.TP
\fB\fICACA_EVENT_ANY \fP\fP
Bitmask for any event\&. 
.SS "enum \fBcaca_key\fP"
Special key values returned by \fBcaca_get_event()\fP for which there is no printable ASCII equivalent\&. 
.PP
\fBEnumerator\fP
.in +1c
.TP
\fB\fICACA_KEY_UNKNOWN \fP\fP
Unknown key\&. 
.TP
\fB\fICACA_KEY_CTRL_A \fP\fP
The Ctrl-A key\&. 
.TP
\fB\fICACA_KEY_CTRL_B \fP\fP
The Ctrl-B key\&. 
.TP
\fB\fICACA_KEY_CTRL_C \fP\fP
The Ctrl-C key\&. 
.TP
\fB\fICACA_KEY_CTRL_D \fP\fP
The Ctrl-D key\&. 
.TP
\fB\fICACA_KEY_CTRL_E \fP\fP
The Ctrl-E key\&. 
.TP
\fB\fICACA_KEY_CTRL_F \fP\fP
The Ctrl-F key\&. 
.TP
\fB\fICACA_KEY_CTRL_G \fP\fP
The Ctrl-G key\&. 
.TP
\fB\fICACA_KEY_BACKSPACE \fP\fP
The backspace key\&. 
.TP
\fB\fICACA_KEY_TAB \fP\fP
The tabulation key\&. 
.TP
\fB\fICACA_KEY_CTRL_J \fP\fP
The Ctrl-J key\&. 
.TP
\fB\fICACA_KEY_CTRL_K \fP\fP
The Ctrl-K key\&. 
.TP
\fB\fICACA_KEY_CTRL_L \fP\fP
The Ctrl-L key\&. 
.TP
\fB\fICACA_KEY_RETURN \fP\fP
The return key\&. 
.TP
\fB\fICACA_KEY_CTRL_N \fP\fP
The Ctrl-N key\&. 
.TP
\fB\fICACA_KEY_CTRL_O \fP\fP
The Ctrl-O key\&. 
.TP
\fB\fICACA_KEY_CTRL_P \fP\fP
The Ctrl-P key\&. 
.TP
\fB\fICACA_KEY_CTRL_Q \fP\fP
The Ctrl-Q key\&. 
.TP
\fB\fICACA_KEY_CTRL_R \fP\fP
The Ctrl-R key\&. 
.TP
\fB\fICACA_KEY_PAUSE \fP\fP
The pause key\&. 
.TP
\fB\fICACA_KEY_CTRL_T \fP\fP
The Ctrl-T key\&. 
.TP
\fB\fICACA_KEY_CTRL_U \fP\fP
The Ctrl-U key\&. 
.TP
\fB\fICACA_KEY_CTRL_V \fP\fP
The Ctrl-V key\&. 
.TP
\fB\fICACA_KEY_CTRL_W \fP\fP
The Ctrl-W key\&. 
.TP
\fB\fICACA_KEY_CTRL_X \fP\fP
The Ctrl-X key\&. 
.TP
\fB\fICACA_KEY_CTRL_Y \fP\fP
The Ctrl-Y key\&. 
.TP
\fB\fICACA_KEY_CTRL_Z \fP\fP
The Ctrl-Z key\&. 
.TP
\fB\fICACA_KEY_ESCAPE \fP\fP
The escape key\&. 
.TP
\fB\fICACA_KEY_DELETE \fP\fP
The delete key\&. 
.TP
\fB\fICACA_KEY_UP \fP\fP
The up arrow key\&. 
.TP
\fB\fICACA_KEY_DOWN \fP\fP
The down arrow key\&. 
.TP
\fB\fICACA_KEY_LEFT \fP\fP
The left arrow key\&. 
.TP
\fB\fICACA_KEY_RIGHT \fP\fP
The right arrow key\&. 
.TP
\fB\fICACA_KEY_INSERT \fP\fP
The insert key\&. 
.TP
\fB\fICACA_KEY_HOME \fP\fP
The home key\&. 
.TP
\fB\fICACA_KEY_END \fP\fP
The end key\&. 
.TP
\fB\fICACA_KEY_PAGEUP \fP\fP
The page up key\&. 
.TP
\fB\fICACA_KEY_PAGEDOWN \fP\fP
The page down key\&. 
.TP
\fB\fICACA_KEY_F1 \fP\fP
The F1 key\&. 
.TP
\fB\fICACA_KEY_F2 \fP\fP
The F2 key\&. 
.TP
\fB\fICACA_KEY_F3 \fP\fP
The F3 key\&. 
.TP
\fB\fICACA_KEY_F4 \fP\fP
The F4 key\&. 
.TP
\fB\fICACA_KEY_F5 \fP\fP
The F5 key\&. 
.TP
\fB\fICACA_KEY_F6 \fP\fP
The F6 key\&. 
.TP
\fB\fICACA_KEY_F7 \fP\fP
The F7 key\&. 
.TP
\fB\fICACA_KEY_F8 \fP\fP
The F8 key\&. 
.TP
\fB\fICACA_KEY_F9 \fP\fP
The F9 key\&. 
.TP
\fB\fICACA_KEY_F10 \fP\fP
The F10 key\&. 
.TP
\fB\fICACA_KEY_F11 \fP\fP
The F11 key\&. 
.TP
\fB\fICACA_KEY_F12 \fP\fP
The F12 key\&. 
.TP
\fB\fICACA_KEY_F13 \fP\fP
The F13 key\&. 
.TP
\fB\fICACA_KEY_F14 \fP\fP
The F14 key\&. 
.TP
\fB\fICACA_KEY_F15 \fP\fP
The F15 key\&. 
.SS "enum \fBCACA_CONIO_COLORS\fP"
This enum lists the colour values for the DOS conio\&.h compatibility layer\&. 
.SS "enum \fBCACA_CONIO_CURSOR\fP"
This enum lists the cursor mode values for the DOS conio\&.h compatibility layer\&. 
.SS "enum \fBCACA_CONIO_MODE\fP"
This enum lists the video mode values for the DOS conio\&.h compatibility layer\&. 
.SH "Author"
.PP 
Generated automatically by Doxygen for libcaca from the source code\&.
