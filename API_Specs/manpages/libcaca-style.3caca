.TH "libcaca-style" 3caca "Sun Feb 7 2016" "Version 0.99.beta19" "libcaca" \" -*- nroff -*-
.ad l
.nh
.SH NAME
libcaca-style \- Libcaca coding style 

.SH "General guidelines"
.PP
A pretty safe rule of thumb is: look at what has already been done and try to do the same\&.
.PP
.IP "\(bu" 2
Tabulations should be avoided and replaced with \fIeight\fP spaces\&.
.IP "\(bu" 2
Indentation is generally 4 spaces\&.
.IP "\(bu" 2
Lines should wrap at most at 79 characters\&.
.IP "\(bu" 2
Do not leave whitespace at the end of lines\&.
.IP "\(bu" 2
Do not use multiple spaces for anything else than indentation\&.
.IP "\(bu" 2
Code qui fait des warnings == code de porc == deux baffes dans ta gueule
.PP
.SH "C coding style"
.PP
Try to use short names whenever possible (\fCi\fP for indices, \fCw\fP for width, \fCcv\fP for canvas\&.\&.\&.)\&. Macros are always uppercase, variable and function names are always lowercase\&. Use the underscore to separate words within names:
.PP
.PP
.nf
#define BROKEN 0
#define MAX(x, y) ((x > y) ? (x) : (y))

unsigned int x, y, w, h;
char *font_name;
void frobulate_every_three_seconds(void);
.fi
.PP
.PP
\fCconst\fP is a \fIsuffix\fP\&. It's \fCchar\fP \fCconst\fP \fC*foo\fP, not \fCconst\fP \fCchar\fP \fC*foo\fP\&.
.PP
Use spaces after commas and between operators\&. Do not use spaces after an opening parenthesis or before a closing one:
.PP
.PP
.nf
a += 2;
b = (a * (c + d));
x = min(x1, x2, x3);
.fi
.PP
.PP
Do not put a space between functions and the corresponding opening parenthesis:
.PP
.PP
.nf
int function(int);
.fi
.PP
.PP
A space can be inserted after keywords such as \fCfor\fP, \fCwhile\fP or \fCif\fP, but consistency with the rest of the page is encouraged:
.PP
.PP
.nf
if(a == b)
    return;

if (p == NULL)
.fi
.PP
.PP
Do not put parentheses around return values:
.PP
.PP
.nf
return a + (b & x) + d[10];
.fi
.PP
.PP
Opening braces should be on a line of their own, aligned with the current block\&. Braces are optional for one-liners:
.PP
.PP
.nf
int function(int a)
{
    if(a & 0x84)
        return a;

    if(a < 0)
    {
        return -a;
    }
    else
    {
        a /= 2;

        switch(a)
        {
            case 0:
            case 1:
                return -1;
                break;
            default:
                return a;
        }
    }
}
.fi
.PP
.SH "C++ coding style"
.PP
Nothing here yet\&. 
