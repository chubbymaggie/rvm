.TH "caca_attributes" 3caca "Sun Feb 7 2016" "Version 0.99.beta19" "libcaca" \" -*- nroff -*-
.ad l
.nh
.SH NAME
caca_attributes \- libcaca attribute conversions
.SS "Functions"

.in +1c
.ti -1c
.RI "__extern uint32_t \fBcaca_get_attr\fP (\fBcaca_canvas_t\fP const *, int, int)"
.br
.RI "\fIGet the text attribute at the given coordinates\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_set_attr\fP (\fBcaca_canvas_t\fP *, uint32_t)"
.br
.RI "\fISet the default character attribute\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_unset_attr\fP (\fBcaca_canvas_t\fP *, uint32_t)"
.br
.RI "\fIUnset flags in the default character attribute\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_toggle_attr\fP (\fBcaca_canvas_t\fP *, uint32_t)"
.br
.RI "\fIToggle flags in the default character attribute\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_put_attr\fP (\fBcaca_canvas_t\fP *, int, int, uint32_t)"
.br
.RI "\fISet the character attribute at the given coordinates\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_set_color_ansi\fP (\fBcaca_canvas_t\fP *, uint8_t, uint8_t)"
.br
.RI "\fISet the default colour pair for text (ANSI version)\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_set_color_argb\fP (\fBcaca_canvas_t\fP *, uint16_t, uint16_t)"
.br
.RI "\fISet the default colour pair for text (truecolor version)\&. \fP"
.ti -1c
.RI "__extern uint8_t \fBcaca_attr_to_ansi\fP (uint32_t)"
.br
.RI "\fIGet DOS ANSI information from attribute\&. \fP"
.ti -1c
.RI "__extern uint8_t \fBcaca_attr_to_ansi_fg\fP (uint32_t)"
.br
.RI "\fIGet ANSI foreground information from attribute\&. \fP"
.ti -1c
.RI "__extern uint8_t \fBcaca_attr_to_ansi_bg\fP (uint32_t)"
.br
.RI "\fIGet ANSI background information from attribute\&. \fP"
.ti -1c
.RI "__extern uint16_t \fBcaca_attr_to_rgb12_fg\fP (uint32_t)"
.br
.RI "\fIGet 12-bit RGB foreground information from attribute\&. \fP"
.ti -1c
.RI "__extern uint16_t \fBcaca_attr_to_rgb12_bg\fP (uint32_t)"
.br
.RI "\fIGet 12-bit RGB background information from attribute\&. \fP"
.ti -1c
.RI "__extern void \fBcaca_attr_to_argb64\fP (uint32_t, uint8_t[8])"
.br
.RI "\fIGet 64-bit ARGB information from attribute\&. \fP"
.in -1c
.SH "Detailed Description"
.PP 
These functions perform conversions between attribute values\&. 
.SH "Function Documentation"
.PP 
.SS "__extern uint32_t caca_get_attr (\fBcaca_canvas_t\fP const * cv, int x, int y)"
Get the internal \fIlibcaca\fP attribute value of the character at the given coordinates\&. The attribute value has 32 significant bits, organised as follows from MSB to LSB:
.IP "\(bu" 2
3 bits for the background alpha
.IP "\(bu" 2
4 bits for the background red component
.IP "\(bu" 2
4 bits for the background green component
.IP "\(bu" 2
3 bits for the background blue component
.IP "\(bu" 2
3 bits for the foreground alpha
.IP "\(bu" 2
4 bits for the foreground red component
.IP "\(bu" 2
4 bits for the foreground green component
.IP "\(bu" 2
3 bits for the foreground blue component
.IP "\(bu" 2
4 bits for the bold, italics, underline and blink flags
.PP
.PP
If the coordinates are outside the canvas boundaries, the current attribute is returned\&.
.PP
This function never fails\&.
.PP
\fBParameters:\fP
.RS 4
\fIcv\fP A handle to the libcaca canvas\&. 
.br
\fIx\fP X coordinate\&. 
.br
\fIy\fP Y coordinate\&. 
.RE
.PP
\fBReturns:\fP
.RS 4
The requested attribute\&. 
.RE
.PP

.PP
Referenced by caca_conio_textbackground(), caca_conio_textcolor(), caca_dither_bitmap(), caca_flush_figlet(), and caca_put_figchar()\&.
.SS "__extern int caca_set_attr (\fBcaca_canvas_t\fP * cv, uint32_t attr)"
Set the default character attribute for drawing\&. Attributes define foreground and background colour, transparency, bold, italics and underline styles, as well as blink\&. String functions such as \fBcaca_printf()\fP and graphical primitive functions such as \fBcaca_draw_line()\fP will use this attribute\&.
.PP
The value of \fIattr\fP is either:
.IP "\(bu" 2
a 32-bit integer as returned by \fBcaca_get_attr()\fP, in which case it also contains colour information,
.IP "\(bu" 2
a combination (bitwise OR) of style values (\fICACA_UNDERLINE\fP, \fICACA_BLINK\fP, \fICACA_BOLD\fP and \fICACA_ITALICS\fP), in which case setting the attribute does not modify the current colour information\&.
.PP
.PP
To retrieve the current attribute value, use caca_get_attr(-1,-1)\&.
.PP
This function never fails\&.
.PP
\fBParameters:\fP
.RS 4
\fIcv\fP A handle to the libcaca canvas\&. 
.br
\fIattr\fP The requested attribute value\&. 
.RE
.PP
\fBReturns:\fP
.RS 4
This function always returns 0\&. 
.RE
.PP

.PP
Referenced by caca_dither_bitmap(), and caca_put_figchar()\&.
.SS "__extern int caca_unset_attr (\fBcaca_canvas_t\fP * cv, uint32_t attr)"
Unset flags in the default character attribute for drawing\&. Attributes define foreground and background colour, transparency, bold, italics and underline styles, as well as blink\&. String functions such as \fBcaca_printf()\fP and graphical primitive functions such as \fBcaca_draw_line()\fP will use this attribute\&.
.PP
The value of \fIattr\fP is a combination (bitwise OR) of style values (\fICACA_UNDERLINE\fP, \fICACA_BLINK\fP, \fICACA_BOLD\fP and \fICACA_ITALICS\fP)\&. Unsetting these attributes does not modify the current colour information\&.
.PP
To retrieve the current attribute value, use caca_get_attr(-1,-1)\&.
.PP
This function never fails\&.
.PP
\fBParameters:\fP
.RS 4
\fIcv\fP A handle to the libcaca canvas\&. 
.br
\fIattr\fP The requested attribute values to unset\&. 
.RE
.PP
\fBReturns:\fP
.RS 4
This function always returns 0\&. 
.RE
.PP

.SS "__extern int caca_toggle_attr (\fBcaca_canvas_t\fP * cv, uint32_t attr)"
Toggle flags in the default character attribute for drawing\&. Attributes define foreground and background colour, transparency, bold, italics and underline styles, as well as blink\&. String functions such as \fBcaca_printf()\fP and graphical primitive functions such as \fBcaca_draw_line()\fP will use this attribute\&.
.PP
The value of \fIattr\fP is a combination (bitwise OR) of style values (\fICACA_UNDERLINE\fP, \fICACA_BLINK\fP, \fICACA_BOLD\fP and \fICACA_ITALICS\fP)\&. Toggling these attributes does not modify the current colour information\&.
.PP
To retrieve the current attribute value, use caca_get_attr(-1,-1)\&.
.PP
This function never fails\&.
.PP
\fBParameters:\fP
.RS 4
\fIcv\fP A handle to the libcaca canvas\&. 
.br
\fIattr\fP The requested attribute values to toggle\&. 
.RE
.PP
\fBReturns:\fP
.RS 4
This function always returns 0\&. 
.RE
.PP

.SS "__extern int caca_put_attr (\fBcaca_canvas_t\fP * cv, int x, int y, uint32_t attr)"
Set the character attribute, without changing the character's value\&. If the character at the given coordinates is a fullwidth character, both cells' attributes are replaced\&.
.PP
The value of \fIattr\fP is either:
.IP "\(bu" 2
a 32-bit integer as returned by \fBcaca_get_attr()\fP, in which case it also contains colour information,
.IP "\(bu" 2
a combination (bitwise OR) of style values (\fICACA_UNDERLINE\fP, \fICACA_BLINK\fP, \fICACA_BOLD\fP and \fICACA_ITALICS\fP), in which case setting the attribute does not modify the current colour information\&.
.PP
.PP
This function never fails\&.
.PP
\fBParameters:\fP
.RS 4
\fIcv\fP A handle to the libcaca canvas\&. 
.br
\fIx\fP X coordinate\&. 
.br
\fIy\fP Y coordinate\&. 
.br
\fIattr\fP The requested attribute value\&. 
.RE
.PP
\fBReturns:\fP
.RS 4
This function always returns 0\&. 
.RE
.PP

.PP
References caca_add_dirty_rect(), and CACA_MAGIC_FULLWIDTH\&.
.PP
Referenced by caca_flush_figlet(), and caca_put_figchar()\&.
.SS "__extern int caca_set_color_ansi (\fBcaca_canvas_t\fP * cv, uint8_t fg, uint8_t bg)"
Set the default ANSI colour pair for text drawing\&. String functions such as \fBcaca_printf()\fP and graphical primitive functions such as \fBcaca_draw_line()\fP will use these attributes\&.
.PP
Color values are those defined in \fBcaca\&.h\fP, such as CACA_RED or CACA_TRANSPARENT\&.
.PP
If an error occurs, -1 is returned and \fBerrno\fP is set accordingly:
.IP "\(bu" 2
\fCEINVAL\fP At least one of the colour values is invalid\&.
.PP
.PP
\fBParameters:\fP
.RS 4
\fIcv\fP A handle to the libcaca canvas\&. 
.br
\fIfg\fP The requested ANSI foreground colour\&. 
.br
\fIbg\fP The requested ANSI background colour\&. 
.RE
.PP
\fBReturns:\fP
.RS 4
0 in case of success, -1 if an error occurred\&. 
.RE
.PP

.PP
Referenced by caca_conio_textbackground(), caca_conio_textcolor(), caca_create_canvas(), and caca_dither_bitmap()\&.
.SS "__extern int caca_set_color_argb (\fBcaca_canvas_t\fP * cv, uint16_t fg, uint16_t bg)"
Set the default ARGB colour pair for text drawing\&. String functions such as \fBcaca_printf()\fP and graphical primitive functions such as \fBcaca_draw_line()\fP will use these attributes\&.
.PP
Colors are 16-bit ARGB values, each component being coded on 4 bits\&. For instance, 0xf088 is solid dark cyan (A=15 R=0 G=8 B=8), and 0x8fff is white with 50% alpha (A=8 R=15 G=15 B=15)\&.
.PP
This function never fails\&.
.PP
\fBParameters:\fP
.RS 4
\fIcv\fP A handle to the libcaca canvas\&. 
.br
\fIfg\fP The requested ARGB foreground colour\&. 
.br
\fIbg\fP The requested ARGB background colour\&. 
.RE
.PP
\fBReturns:\fP
.RS 4
This function always returns 0\&. 
.RE
.PP

.SS "__extern uint8_t caca_attr_to_ansi (uint32_t attr)"
Get the ANSI colour pair for a given attribute\&. The returned value is an 8-bit value whose higher 4 bits are the background colour and lower 4 bits are the foreground colour\&.
.PP
If the attribute has ARGB colours, the nearest colour is used\&. Special attributes such as \fICACA_DEFAULT\fP and \fICACA_TRANSPARENT\fP are not handled and are both replaced with \fICACA_LIGHTGRAY\fP for the foreground colour and \fICACA_BLACK\fP for the background colour\&.
.PP
This function never fails\&. If the attribute value is outside the expected 32-bit range, higher order bits are simply ignored\&.
.PP
\fBParameters:\fP
.RS 4
\fIattr\fP The requested attribute value\&. 
.RE
.PP
\fBReturns:\fP
.RS 4
The corresponding DOS ANSI value\&. 
.RE
.PP

.PP
References CACA_BLACK, and CACA_LIGHTGRAY\&.
.SS "__extern uint8_t caca_attr_to_ansi_fg (uint32_t attr)"
Get the ANSI foreground colour value for a given attribute\&. The returned value is either one of the \fICACA_RED\fP, \fICACA_BLACK\fP etc\&. predefined colours, or the special value \fICACA_DEFAULT\fP meaning the media's default foreground value, or the special value \fICACA_TRANSPARENT\fP\&.
.PP
If the attribute has ARGB colours, the nearest colour is returned\&.
.PP
This function never fails\&. If the attribute value is outside the expected 32-bit range, higher order bits are simply ignored\&.
.PP
\fBParameters:\fP
.RS 4
\fIattr\fP The requested attribute value\&. 
.RE
.PP
\fBReturns:\fP
.RS 4
The corresponding ANSI foreground value\&. 
.RE
.PP

.PP
Referenced by caca_conio_textbackground()\&.
.SS "__extern uint8_t caca_attr_to_ansi_bg (uint32_t attr)"
Get the ANSI background colour value for a given attribute\&. The returned value is either one of the \fICACA_RED\fP, \fICACA_BLACK\fP etc\&. predefined colours, or the special value \fICACA_DEFAULT\fP meaning the media's default background value, or the special value \fICACA_TRANSPARENT\fP\&.
.PP
If the attribute has ARGB colours, the nearest colour is returned\&.
.PP
This function never fails\&. If the attribute value is outside the expected 32-bit range, higher order bits are simply ignored\&.
.PP
\fBParameters:\fP
.RS 4
\fIattr\fP The requested attribute value\&. 
.RE
.PP
\fBReturns:\fP
.RS 4
The corresponding ANSI background value\&. 
.RE
.PP

.PP
Referenced by caca_conio_textcolor()\&.
.SS "__extern uint16_t caca_attr_to_rgb12_fg (uint32_t attr)"
Get the 12-bit foreground colour value for a given attribute\&. The returned value is a native-endian encoded integer with each red, green and blue values encoded on 8 bits in the following order:
.IP "\(bu" 2
8-11 most significant bits: red
.IP "\(bu" 2
4-7 most significant bits: green
.IP "\(bu" 2
least significant bits: blue
.PP
.PP
This function never fails\&. If the attribute value is outside the expected 32-bit range, higher order bits are simply ignored\&.
.PP
\fBParameters:\fP
.RS 4
\fIattr\fP The requested attribute value\&. 
.RE
.PP
\fBReturns:\fP
.RS 4
The corresponding 12-bit RGB foreground value\&. 
.RE
.PP

.PP
References CACA_DEFAULT, CACA_LIGHTGRAY, and CACA_TRANSPARENT\&.
.SS "__extern uint16_t caca_attr_to_rgb12_bg (uint32_t attr)"
Get the 12-bit background colour value for a given attribute\&. The returned value is a native-endian encoded integer with each red, green and blue values encoded on 8 bits in the following order:
.IP "\(bu" 2
8-11 most significant bits: red
.IP "\(bu" 2
4-7 most significant bits: green
.IP "\(bu" 2
least significant bits: blue
.PP
.PP
This function never fails\&. If the attribute value is outside the expected 32-bit range, higher order bits are simply ignored\&.
.PP
\fBParameters:\fP
.RS 4
\fIattr\fP The requested attribute value\&. 
.RE
.PP
\fBReturns:\fP
.RS 4
The corresponding 12-bit RGB background value\&. 
.RE
.PP

.PP
References CACA_BLACK, CACA_DEFAULT, and CACA_TRANSPARENT\&.
.SS "__extern void caca_attr_to_argb64 (uint32_t attr, uint8_t argb[8])"
Get the 64-bit colour and alpha values for a given attribute\&. The values are written as 8-bit integers in the \fIargb\fP array in the following order:
.IP "\(bu" 2
\fIargb\fP[0]: background alpha value
.IP "\(bu" 2
\fIargb\fP[1]: background red value
.IP "\(bu" 2
\fIargb\fP[2]: background green value
.IP "\(bu" 2
\fIargb\fP[3]: background blue value
.IP "\(bu" 2
\fIargb\fP[4]: foreground alpha value
.IP "\(bu" 2
\fIargb\fP[5]: foreground red value
.IP "\(bu" 2
\fIargb\fP[6]: foreground green value
.IP "\(bu" 2
\fIargb\fP[7]: foreground blue value
.PP
.PP
This function never fails\&. If the attribute value is outside the expected 32-bit range, higher order bits are simply ignored\&.
.PP
\fBParameters:\fP
.RS 4
\fIattr\fP The requested attribute value\&. 
.br
\fIargb\fP An array of 8-bit integers\&. 
.RE
.PP

.PP
References CACA_BLACK, CACA_DEFAULT, CACA_LIGHTGRAY, and CACA_TRANSPARENT\&.
.PP
Referenced by caca_render_canvas()\&.
.SH "Author"
.PP 
Generated automatically by Doxygen for libcaca from the source code\&.
