.TH "libcaca" 3caca "Sun Feb 7 2016" "Version 0.99.beta19" "libcaca" \" -*- nroff -*-
.ad l
.nh
.SH NAME
libcaca \- libcaca basic functions
.SS "Functions"

.in +1c
.ti -1c
.RI "__extern \fBcaca_canvas_t\fP * \fBcaca_create_canvas\fP (int, int)"
.br
.RI "\fIInitialise a \fIlibcaca\fP canvas\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_manage_canvas\fP (\fBcaca_canvas_t\fP *, int(*)(void *), void *)"
.br
.RI "\fIManage a canvas\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_unmanage_canvas\fP (\fBcaca_canvas_t\fP *, int(*)(void *), void *)"
.br
.RI "\fIunmanage a canvas\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_set_canvas_size\fP (\fBcaca_canvas_t\fP *, int, int)"
.br
.RI "\fIResize a canvas\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_get_canvas_width\fP (\fBcaca_canvas_t\fP const *)"
.br
.RI "\fIGet the canvas width\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_get_canvas_height\fP (\fBcaca_canvas_t\fP const *)"
.br
.RI "\fIGet the canvas height\&. \fP"
.ti -1c
.RI "__extern uint32_t const * \fBcaca_get_canvas_chars\fP (\fBcaca_canvas_t\fP const *)"
.br
.RI "\fIGet the canvas character array\&. \fP"
.ti -1c
.RI "__extern uint32_t const * \fBcaca_get_canvas_attrs\fP (\fBcaca_canvas_t\fP const *)"
.br
.RI "\fIGet the canvas attribute array\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_free_canvas\fP (\fBcaca_canvas_t\fP *)"
.br
.RI "\fIFree a \fIlibcaca\fP canvas\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_rand\fP (int, int)"
.br
.ti -1c
.RI "__extern char const * \fBcaca_get_version\fP (void)"
.br
.RI "\fIReturn the \fIlibcaca\fP version\&. \fP"
.in -1c
.SH "Detailed Description"
.PP 
These functions provide the basic \fIlibcaca\fP routines for library initialisation, system information retrieval and configuration\&. 
.SH "Function Documentation"
.PP 
.SS "__extern \fBcaca_canvas_t\fP* caca_create_canvas (int width, int height)"
Initialise internal \fIlibcaca\fP structures and the backend that will be used for subsequent graphical operations\&. It must be the first \fIlibcaca\fP function to be called in a function\&. \fBcaca_free_canvas()\fP should be called at the end of the program to free all allocated resources\&.
.PP
Both the cursor and the canvas' handle are initialised at the top-left corner\&.
.PP
If an error occurs, NULL is returned and \fBerrno\fP is set accordingly:
.IP "\(bu" 2
\fCEINVAL\fP Specified width or height is invalid\&.
.IP "\(bu" 2
\fCENOMEM\fP Not enough memory for the requested canvas size\&.
.PP
.PP
\fBParameters:\fP
.RS 4
\fIwidth\fP The desired canvas width 
.br
\fIheight\fP The desired canvas height 
.RE
.PP
\fBReturns:\fP
.RS 4
A libcaca canvas handle upon success, NULL if an error occurred\&. 
.RE
.PP

.PP
References CACA_DEFAULT, caca_set_color_ansi(), and CACA_TRANSPARENT\&.
.PP
Referenced by caca_conio_movetext(), caca_create_display_with_driver(), caca_export_area_to_memory(), caca_import_area_from_file(), caca_import_area_from_memory(), and caca_set_canvas_boundaries()\&.
.SS "__extern int caca_manage_canvas (\fBcaca_canvas_t\fP * cv, int(*)(void *) callback, void * p)"
Lock a canvas to prevent it from being resized\&. If non-NULL, the \fIcallback\fP function pointer will be called upon each \fIcaca_set_canvas_size\fP call and if the returned value is zero, the canvas resize request will be denied\&.
.PP
This function is only useful for display drivers such as the \fIlibcaca\fP library\&.
.PP
If an error occurs, -1 is returned and \fBerrno\fP is set accordingly:
.IP "\(bu" 2
\fCEBUSY\fP The canvas is already being managed\&.
.PP
.PP
\fBParameters:\fP
.RS 4
\fIcv\fP A libcaca canvas\&. 
.br
\fIcallback\fP An optional callback function pointer\&. 
.br
\fIp\fP The argument to be passed to \fIcallback\fP\&. 
.RE
.PP
\fBReturns:\fP
.RS 4
0 in case of success, -1 if an error occurred\&. 
.RE
.PP

.PP
Referenced by caca_create_display_with_driver()\&.
.SS "__extern int caca_unmanage_canvas (\fBcaca_canvas_t\fP * cv, int(*)(void *) callback, void * p)"
unlock a canvas previously locked by \fBcaca_manage_canvas()\fP\&. for safety reasons, the callback and callback data arguments must be the same as for the \fBcaca_manage_canvas()\fP call\&.
.PP
this function is only useful for display drivers such as the \fIlibcaca\fP library\&.
.PP
if an error occurs, -1 is returned and \fBerrno\fP is set accordingly:
.IP "\(bu" 2
\fCeinval\fP the canvas is not managed, or the callback arguments do not match\&.
.PP
.PP
\fBParameters:\fP
.RS 4
\fIcv\fP a libcaca canvas\&. 
.br
\fIcallback\fP the \fIcallback\fP argument previously passed to \fBcaca_manage_canvas()\fP\&. 
.br
\fIp\fP the \fIp\fP argument previously passed to \fBcaca_manage_canvas()\fP\&. 
.RE
.PP
\fBReturns:\fP
.RS 4
0 in case of success, -1 if an error occurred\&. 
.RE
.PP

.PP
Referenced by caca_create_display_with_driver(), and caca_free_display()\&.
.SS "__extern int caca_set_canvas_size (\fBcaca_canvas_t\fP * cv, int width, int height)"
Set the canvas' width and height, in character cells\&.
.PP
The contents of the canvas are preserved to the extent of the new canvas size\&. Newly allocated character cells at the right and/or at the bottom of the canvas are filled with spaces\&.
.PP
If as a result of the resize the cursor coordinates fall outside the new canvas boundaries, they are readjusted\&. For instance, if the current X cursor coordinate is 11 and the requested width is 10, the new X cursor coordinate will be 10\&.
.PP
It is an error to try to resize the canvas if an output driver has been attached to the canvas using \fBcaca_create_display()\fP\&. You need to remove the output driver using \fBcaca_free_display()\fP before you can change the canvas size again\&. However, the caca output driver can cause a canvas resize through user interaction\&. See the \fBcaca_event()\fP documentation for more about this\&.
.PP
If an error occurs, -1 is returned and \fBerrno\fP is set accordingly:
.IP "\(bu" 2
\fCEINVAL\fP Specified width or height is invalid\&.
.IP "\(bu" 2
\fCEBUSY\fP The canvas is in use by a display driver and cannot be resized\&.
.IP "\(bu" 2
\fCENOMEM\fP Not enough memory for the requested canvas size\&. If this happens, the canvas handle becomes invalid and should not be used\&.
.PP
.PP
\fBParameters:\fP
.RS 4
\fIcv\fP A libcaca canvas\&. 
.br
\fIwidth\fP The desired canvas width\&. 
.br
\fIheight\fP The desired canvas height\&. 
.RE
.PP
\fBReturns:\fP
.RS 4
0 in case of success, -1 if an error occurred\&. 
.RE
.PP

.PP
Referenced by caca_canvas_set_figfont(), caca_flush_figlet(), and caca_put_figchar()\&.
.SS "__extern int caca_get_canvas_width (\fBcaca_canvas_t\fP const * cv)"
Return the current canvas' width, in character cells\&.
.PP
This function never fails\&.
.PP
\fBParameters:\fP
.RS 4
\fIcv\fP A libcaca canvas\&. 
.RE
.PP
\fBReturns:\fP
.RS 4
The canvas width\&. 
.RE
.PP

.PP
Referenced by caca_conio_clreol(), caca_conio_movetext(), and caca_get_mouse_x()\&.
.SS "__extern int caca_get_canvas_height (\fBcaca_canvas_t\fP const * cv)"
Returns the current canvas' height, in character cells\&.
.PP
This function never fails\&.
.PP
\fBParameters:\fP
.RS 4
\fIcv\fP A libcaca canvas\&. 
.RE
.PP
\fBReturns:\fP
.RS 4
The canvas height\&. 
.RE
.PP

.PP
Referenced by caca_flush_figlet(), and caca_get_mouse_y()\&.
.SS "__extern uint32_t const* caca_get_canvas_chars (\fBcaca_canvas_t\fP const * cv)"
Return the current canvas' internal character array\&. The array elements consist in native endian 32-bit Unicode values as returned by \fBcaca_get_char()\fP\&.
.PP
This function is probably only useful for \fIlibcaca\fP 's internal display drivers\&.
.PP
This function never fails\&.
.PP
\fBParameters:\fP
.RS 4
\fIcv\fP A libcaca canvas\&. 
.RE
.PP
\fBReturns:\fP
.RS 4
The canvas character array\&. 
.RE
.PP

.SS "__extern uint32_t const* caca_get_canvas_attrs (\fBcaca_canvas_t\fP const * cv)"
Returns the current canvas' internal attribute array\&. The array elements consist in native endian 32-bit attribute values as returned by \fBcaca_get_attr()\fP\&.
.PP
This function is probably only useful for \fIlibcaca\fP 's internal display drivers\&.
.PP
This function never fails\&.
.PP
\fBParameters:\fP
.RS 4
\fIcv\fP A libcaca canvas\&. 
.RE
.PP
\fBReturns:\fP
.RS 4
The canvas attribute array\&. 
.RE
.PP

.SS "__extern int caca_free_canvas (\fBcaca_canvas_t\fP * cv)"
Free all resources allocated by \fBcaca_create_canvas()\fP\&. The canvas pointer becomes invalid and must no longer be used unless a new call to \fBcaca_create_canvas()\fP is made\&.
.PP
If an error occurs, -1 is returned and \fBerrno\fP is set accordingly:
.IP "\(bu" 2
\fCEBUSY\fP The canvas is in use by a display driver and cannot be freed\&.
.PP
.PP
\fBParameters:\fP
.RS 4
\fIcv\fP A libcaca canvas\&. 
.RE
.PP
\fBReturns:\fP
.RS 4
0 in case of success, -1 if an error occurred\&. 
.RE
.PP

.PP
References caca_canvas_set_figfont()\&.
.PP
Referenced by caca_canvas_set_figfont(), caca_create_display_with_driver(), caca_export_area_to_memory(), caca_free_display(), caca_import_area_from_file(), and caca_import_area_from_memory()\&.
.SS "__extern char const* caca_get_version (void)"
Return a read-only string with the \fIlibcaca\fP version information\&.
.PP
This function never fails\&.
.PP
\fBReturns:\fP
.RS 4
The \fIlibcaca\fP version information\&. 
.RE
.PP

.SH "Author"
.PP 
Generated automatically by Doxygen for libcaca from the source code\&.
