# -*- coding: utf-8 -*-
import scrapy, sys
reload(sys)
sys.setdefaultencoding('utf8')

class APISpecSpider(scrapy.Spider):
    name = "api_spec"

    def __init__(self):
	self.API_type = ''

    def start_requests(self):
        urls = [
            'https://docs.microsoft.com/en-us/windows/desktop/api/index',
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
	print >>sys.stderr, '# 1', response.url
	for r in response.css("a"):
		href = r.xpath('@href').extract()
		for h in href:
			if h.find('index') > 0:
				next_page = response.urljoin(h)
				yield scrapy.Request(next_page, callback=self.parse2)

    def parse2(self, response):
	# each category
	print >>sys.stderr, '# 2', response.url
	for r in response.css("a"):
		#text = r.xpath('@text').extract()
		for t in r.css('::text').extract():
			#print '\t', t
			# debug
			#if t.find('wsdevlicensing.h') >= 0:
			if t.find('.h') > 0:
				print '# URL', response.url
				print '# Category', response.xpath("/html/head/meta[@property='og:title']/@content").extract()[0]
				print '# File', t
				print '#'
				h = r.xpath('@href').extract()
				#print '\t', t, h
				for ref in h:
					next_page = response.urljoin(ref)
					yield scrapy.Request(next_page, callback=self.parse3)

    def parse3(self, response):
	# each header file
	print >>sys.stderr, '# 3', response.url
	for r in response.xpath('//td'):
		h = r.css('a')
		# we should skip enumerations and other information
		for l in h:
			refs = l.xpath('@href').extract()
			for p in refs:
				#print '\t', p
				next_page = response.urljoin(p)
				yield scrapy.Request(next_page, callback=self.parse4)

    def parse4(self, response):
	print >>sys.stderr, '# 4', response.url, response.xpath('//title')
	title = response.xpath('//title/text()').extract()
	for t in title:
		if t.find('function') >= 0:
			print '# URL', response.url
			func = t[:t.find('function')]
			print '# Function', func
			for c in response.xpath('//code'):
				l = c.css('::text').extract()
				print l[0]
				# return value description
				desc = ''
				for x in response.xpath("//h2[@id='return-value']/following-sibling::p").css('::text').extract():
					if desc == '':
						desc = x
					else:
						desc += x
				print '#', desc 
				# return value samples
				tab = response.xpath("//h2[@id='return-value']/following-sibling::table")
				header = ''
				for h in tab[0].xpath('//th/text()'):
					if header == '':
						header = h.extract()
					else:
						header += ',' + h.extract()
				print '# Header', header
				for tr in response.xpath("//h2[@id='return-value']/following-sibling::table/tr"):
					cell1 = ''
					for i in tr.xpath('td[1]/text()'):
						if cell1 == '':
							cell1 = i.extract()
						else:
							cell1 += i.extract() 
					cell2 = ''
					for i in tr.xpath('td[2]/text()'):
						if cell2 == '':
							cell2 = i.extract()
						else:
							cell2 += i.extract() 
					if cell1 or cell2:
						print '#', func, '==', cell1.strip() + ' ; ' + cell2.strip()
				# we only need the function prototype
				break
				#print '\t', l[0].replace('\n', '')

