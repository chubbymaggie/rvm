#!/usr/bin/python
# Instrument SWRR

import sys, struct
from shutil import copyfile

def usage():
	print 'Usage: insSWRR binary SWRR'
	sys.exit()

def pack_hex_string(signature):
	pattern = [1 for _ in range(len(signature))]
	buf = bytearray(pattern)
	#print len(buf)
	i = 0
	for s in signature:
		if s[-1] == 'L':
			s = s[:-1]
		struct.pack_into('<B', buf, i, int(s, 0))
		i += 1
	return buf

if len(sys.argv) < 3:
	usage()
binary = sys.argv[1]
SWRRFile = sys.argv[2]
inp = open(SWRRFile, 'r')
# Line 1: function_name entry_address byte1 byte2 ... byteN
SWRR = inp.readline().split()[2:]
# Line 2: "signature:" byte1 byte2 ... byteN
signature = inp.readline().split()[1:]
inp.close()
buf = pack_hex_string(signature)
with open(binary, 'rb') as f:
	s = f.read()
offset = s.find(buf)
if offset >= 0:
	copyfile(binary, binary + '.org')
	outp = open(binary, 'r+b')
	outp.seek(offset)
	SWRR_bytes = pack_hex_string(SWRR)
	outp.write(SWRR_bytes)
	outp.close()
	print 'SWRR successfully wrote to offset', offset, 'of', binary
else:
	print >>sys.stderr, 'Error: The signature cannot be found!'

