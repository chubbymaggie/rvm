#!/usr/bin/python
import sys, angr, defanalysis, struct, time, archinfo

def usage():
	print 'Usage: genSWRR binary_file functiona_name SWRR_file symbol_file'
	print 'Example: genSWRR.py webvw.dll CWebViewFolderIcon::setSlice webvw.dll.SWRRs webvw.dll.syms'
	sys.exit()

def load_SWRRs(name, func):
	inp = open(name ,'r')
	while True:
		line = inp.readline()
		if not line:
			break
		line = line.strip()
		if not line:
			continue
		if line.split()[0] == func:
			return line
	inp.close()
	print >>sys.stderr, 'Error:', func, 'not found in SWRR file!'
	return None
		
def load_Symbols(name):
	syms = {}
	inp = open(name ,'r')
	while True:
		line = inp.readline()
		if not line:
			break
		line = line.strip()
		if not line:
			continue
		syms[line.split()[1]] = line.split()[0]
	inp.close()
	return syms

def makeSWRR(func, entry, ret_value, cfg):
	entry_addr = int('0x'+entry, 0)
	cfg_func = cfg.kb.functions.function(addr=entry_addr)
	if cfg_func is None:
		print >>sys.stderr, 'Error:', func, 'cannot be located in binary!'
		return
	else:
		ret = cfg.get_any_node(cfg_func.ret_sites[0].addr)
		analysis = defanalysis.DefAnalysis(cfg, cfg_func)
		analysis.get_def_for_block(ret.addr)
		first_inst_addr = int(analysis.addrs[0], 0)
		last_inst_addr = int(analysis.addrs[-1], 0)
		ret_inst_size = ret.size - last_inst_addr - first_inst_addr
		#print hex(last_inst_addr), ret_inst_size
		if ret_value == 'NULL':
			ret_value = 0
		value = struct.pack('<I', int(ret_value))
		SWRR_str = '0xb8' # MOV EAX, ####
		for i in value:
			SWRR_str += ' '+ hex(ord(i))
		k = 0
		for i in ret.byte_string:
			if ret.addr + k >= last_inst_addr:
				SWRR_str += ' ' + hex(ord(i))
			k += 1
		# output the function name and bytes representing the instructions of the SWRR
		print func, hex(entry_addr), SWRR_str
		# output 32 bytes starting from the entry address of the function
		ins = 'signature:'
		state = proj.factory.blank_state()
		for i in range(32):
			bv = state.memory.load(entry_addr + i, 1, endness=archinfo.Endness.LE)
			ins += ' ' + hex(state.solver.eval(bv))
		print ins

start_time = time.time()
if len(sys.argv) < 5:
	usage()
Binary = sys.argv[1]
FuncName = sys.argv[2]
SWRRFile = sys.argv[3]
SymbolFile = sys.argv[4]

Syms = load_Symbols(SymbolFile)
line = load_SWRRs(SWRRFile, FuncName)
if line is None:
	sys.exit()
proj = angr.Project(Binary, auto_load_libs = False)
cfg = proj.analyses.CFGFast()
parts = line.split()
l = (len(parts) - 1)/2
for i in range(l):
	func = parts[i*2+1]
	ret_value = parts[i*2 + 2]
	#print >>sys.stderr, func
	#print >>sys.stderr, ret_value
	makeSWRR(func, Syms[func], ret_value, cfg)
end_time = time.time()
print 'genSWRR time:', end_time - start_time
